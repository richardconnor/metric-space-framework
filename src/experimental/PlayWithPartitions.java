package experimental;

import java.util.List;

import searchStructures.ObjectWithDistance;
import searchStructures.Quicksort;
import sisap_2017_experiments.NdimSimplex;
import testloads.TestContext;
import testloads.TestContext.Context;
import coreConcepts.Metric;
import eu.similarity.msc.data.cartesian.CartesianPoint;

public class PlayWithPartitions {

	public static void main(String[] a) throws Exception {
		TestContext tl = new TestContext(Context.colors);

		double thresh = tl.getThreshold();
		List<CartesianPoint> dat = tl.getData();
		CartesianPoint p_1 = dat.get(1003);
		CartesianPoint p_2 = dat.get(1004);
		CartesianPoint p1 = dat.get(1005);
		CartesianPoint p2 = dat.get(1006);
		
		System.out.println(tl.metric().distance(p_1, p_2));
		System.out.println(tl.metric().distance(p1, p2));
		final Metric<CartesianPoint> metric = tl.metric();
		NdimSimplex<CartesianPoint> simp = new NdimSimplex<>(metric, p1, p2);

		final List<CartesianPoint> subList = dat.subList(0, 1000);
		@SuppressWarnings("unchecked")
		ObjectWithDistance<Object>[] xOffsets = new ObjectWithDistance[subList
				.size()];
		int ptr = 0;
		for (CartesianPoint s : subList) {
			double[] apex = simp.getApex(s);
			ObjectWithDistance<Object> obj = new ObjectWithDistance<>(null,
					apex[0]);
			xOffsets[ptr++] = obj;
		}
		Quicksort.sort(xOffsets);

		final double min = xOffsets[0].getDistance(); // can be negative
		final double max = xOffsets[xOffsets.length - 1].getDistance();

		double[] excProbs = new double[1000];
		double[] midVals = new double[1000];
		int leftPtr = 0;
		int midPtr = 0;
		int rightPtr = 0;
		do {
			while (midPtr < xOffsets.length
					&& xOffsets[midPtr].getDistance() < xOffsets[leftPtr]
							.getDistance() + thresh / 2) {
				midPtr++;
			}
			while (rightPtr < xOffsets.length
					&& xOffsets[rightPtr].getDistance() < xOffsets[leftPtr]
							.getDistance() + thresh) {
				rightPtr++;
			}

			if (midPtr < 1000) {
				midVals[midPtr] = xOffsets[midPtr].getDistance();
				double leftProb = leftPtr / 1000.0;
				double rightProb = (999 - rightPtr) / 1000.0;
				double leftMass = midPtr / 1000.0;
				double rightMass = (999 - midPtr) / 1000.0;
				excProbs[midPtr] = leftProb * rightMass + rightProb * leftMass;
			}

			leftPtr++;
		} while (midPtr < xOffsets.length);

		int ptr0 = 0;
		for (double d : excProbs) {
			System.out.println(midVals[ptr0++] + "\t" + d);
		}
	}

	private static void createHistogram(ObjectWithDistance<Object>[] xOffsets) {

		final double min = xOffsets[0].getDistance(); // can be negative
		final double max = xOffsets[xOffsets.length - 1].getDistance();

		// eg -0.9 - 4.1, max = 5.0
		// inc = 0.05
		// so dist at max- gives 99 - 0
		// dist at min gives 99 - (inc * 100)
		double inc = (max - min) / 100;

		int[] histo = new int[100];

		for (ObjectWithDistance o : xOffsets) {
			double dist = o.getDistance();
			double bin = 100 - (int) Math.floor((max - dist) / inc);
		}
	}
}
