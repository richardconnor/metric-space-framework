package searchStructures;

import java.util.List;

import coreConcepts.Metric;

public class SearchStructure<T> {

	protected Metric<T> metric;
	protected List<T> data;

	public SearchStructure(List<T> data, Metric<T> metric) {
		this.metric = metric;
		this.data = data;
	}

}
