package searchStructures;

import java.util.ArrayList;
import java.util.List;

import coreConcepts.Metric;

public class KDTree extends SearchIndex<double[]> {

	TreeNode root;
	int data_dimension;

	private class TreeNode {
		double[] head;
		int dimension;
		boolean isLeaf;
		TreeNode left, right;

		TreeNode(List<double[]> data, int dimension) {
			this.dimension = dimension;
			if (data.size() == 1) {
				this.isLeaf = true;
				this.head = data.get(0);
			} else {
				List<double[]> leftList = new ArrayList<>();
				List<double[]> rightList = new ArrayList<>();
				@SuppressWarnings("unchecked")
				ObjectWithDistance<double[]>[] owds = new ObjectWithDistance[data
						.size()];
				for (int i = 0; i < data.size(); i++) {
					double[] d = data.get(i);
					owds[i] = new ObjectWithDistance<>(d, d[this.dimension]);
				}
				Quicksort.placeMedian(owds);
				final int medPos = owds.length / 2;
				this.head = owds[medPos].getValue();
				for (int i = 0; i < medPos; i++) {
					leftList.add(owds[i].getValue());
				}
				for (int i = medPos + 1; i < owds.length; i++) {
					rightList.add(owds[i].getValue());
				}
				final int newDim = (dimension + 1) % KDTree.this.data_dimension;
				if (leftList.size() != 0) {
					this.left = new TreeNode(leftList, newDim);
				}
				if (rightList.size() != 0) {
					this.right = new TreeNode(rightList, newDim);
				}
			}
		}

		public int cardinality() {
			if (this.isLeaf) {
				return 1;
			} else {
				int c1 = this.left != null ? this.left.cardinality() : 0;
				int c2 = this.right != null ? this.right.cardinality() : 0;
				return 1 + c1 + c2;
			}
		}

		public void thesholdSearch(double[] query, double t,
				List<double[]> results) {
			if (metric.distance(this.head, query) <= t) {
				results.add(this.head);
			}
			double split = head[dimension];
			double qVal = query[dimension];
			final boolean canExcludeLeft = qVal - split > t;
			final boolean canExcludeRight = split - qVal > t;
			if (this.left != null && !canExcludeLeft) {
				this.left.thesholdSearch(query, t, results);
			}
			if (this.right != null && !canExcludeRight) {
				this.right.thesholdSearch(query, t, results);
			}
		}
	}

	public KDTree(List<double[]> data, Metric<double[]> metric) {
		super(data, metric);
		this.data_dimension = data.get(0).length;
		this.root = new TreeNode(data, 0);
	}

	@Override
	public List<double[]> thresholdSearch(double[] query, double t) {
		List<double[]> res = new ArrayList<>();
		this.root.thesholdSearch(query, t, res);
		return res;
	}

	@Override
	public String getShortName() {
		return "kdt";
	}

	public static Metric<double[]> l2() {
		Metric<double[]> m = new Metric<double[]>() {

			@Override
			public double distance(double[] x, double[] y) {
				double acc = 0;
				for (int i = 0; i < x.length; i++) {
					double diff = x[i] - y[i];
					acc += diff * diff;
				}
				return Math.sqrt(acc);
			}

			@Override
			public String getMetricName() {
				return "l2";
			}
		};
		return m;
	}

}
