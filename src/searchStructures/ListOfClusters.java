package searchStructures;

import java.util.ArrayList;
import java.util.List;

import coreConcepts.Metric;

public class ListOfClusters<T> extends SearchIndex<T> {

	private int clusterSize;
	private ListNode entryPoint;

	class ListNode {

		T head;
		double cr;
		List<T> cluster;
		ListNode next;

		@SuppressWarnings({ "synthetic-access" })
		ListNode(List<T> data) {
			int dSize = data.size();
			this.head = data.get(0);
			this.cr = 0;
			if (dSize <= ListOfClusters.this.clusterSize) {
				for (int i = 1; i < data.size(); i++) {
					final T d = data.get(i);
					final double distance = ListOfClusters.this.metric
							.distance(this.head, d);
					if (distance > this.cr) {
						this.cr = distance;
					}
				}
				this.cluster = data.subList(1, data.size());
			} else {
				@SuppressWarnings("unchecked")
				ObjectWithDistance<T>[] owds = new ObjectWithDistance[dSize - 1];
				for (int i = 1; i < dSize; i++) {
					final T dat = data.get(i);
					final double distance = ListOfClusters.this.metric
							.distance(dat, this.head);
					owds[i - 1] = new ObjectWithDistance<>(dat, distance);
				}
				Quicksort.placeOrdinal(owds,
						ListOfClusters.this.clusterSize - 1);
				this.cluster = new ArrayList<>();
				for (int i = 0; i <= ListOfClusters.this.clusterSize; i++) {
					this.cluster.add(owds[i].getValue());
				}

				this.cr = owds[ListOfClusters.this.clusterSize].getDistance();

				List<T> nextData = new ArrayList<>();
				for (int i = ListOfClusters.this.clusterSize + 1; i < owds.length; i++) {
					nextData.add(owds[i].getValue());
				}
				if (nextData.size() != 0) {
					this.next = new ListNode(nextData);
				}
			}
		}

		public int cardinality() {
			if (this.next == null) {
				return 1 + this.cluster.size();
			} else {
				return 1 + this.cluster.size() + this.next.cardinality();
			}
		}

		public void thresholdQuery(T q, double threshold, List<T> res) {

			double qTohDist = ListOfClusters.this.metric.distance(q, this.head);

			if (qTohDist <= this.cr + threshold) {
				// ie we need to search the node

				// silly technicality; this is almost free, but can only be true
				// if we're in this code...
				if (qTohDist <= threshold) {
					res.add(this.head);
				}
				for (T p : this.cluster) {
					if (ListOfClusters.this.metric.distance(q, p) <= threshold) {
						res.add(p);
					}
				}

			}
			if (this.next != null && !(qTohDist < this.cr - threshold)) {
				this.next.thresholdQuery(q, threshold, res);
			}

		}
	}

	public ListOfClusters(List<T> data, Metric<T> metric, int clustersize) {
		super(data, metric);
		// start with a stupid algorithm and take it from there...
		this.clusterSize = clustersize;

		this.entryPoint = new ListNode(data);

	}

	@Override
	public List<T> thresholdSearch(T query, double t) {
		List<T> res = new ArrayList<>();
		this.entryPoint.thresholdQuery(query, t, res);
		return res;
	}

	@Override
	public String getShortName() {
		return "loc_" + this.clusterSize;
	}
}
