package searchStructures;

import java.util.ArrayList;
import java.util.List;

import coreConcepts.Metric;

public class Laesa<T> extends SearchIndex<T> {

	private List<T> refPoints;
	double[][] refDists;

	public Laesa(List<T> data, List<T> refPoints, Metric<T> metric) {
		super(data, metric);
		this.refPoints = refPoints;
		this.refDists = new double[data.size()][refPoints.size()];
		int refPtr = 0;
		int datPtr = 0;
		for (T rPoint : refPoints) {
			for (T dPoint : data) {
				this.refDists[datPtr][refPtr] = this.metric.distance(rPoint, dPoint);
				datPtr++;
			}
			refPtr++;
		}
	}

	@Override
	public List<T> thresholdSearch(T query, double t) {
		List<T> res = new ArrayList<>();
		double[] qDists = new double[this.refPoints.size()];
		int refPtr = 0;
		for (T rPoint : this.refPoints) {
			qDists[refPtr++] = this.metric.distance(rPoint, query);
		}
		int dPtr = 0;
		for (T datum : this.data) {
			if (!canExclude(qDists, this.refDists[dPtr++], t)) {
				if (this.metric.distance(query, datum) <= t) {
					res.add(datum);
				}
			}
		}

		return res;
	}

	private static boolean canExclude(double[] qDists, double[] rDists, double t) {
		boolean excluded = false;
		for (int i = 0; i < qDists.length; i++) {
			if (qDists[i] > rDists[i] + t) {
				excluded = true;
			}
			if (rDists[i] > qDists[i] + t) {
				excluded = true;
			}
		}
		return excluded;
	}

	@Override
	public String getShortName() {
		return "laesa";
	}

}
