package searchStructures.experimental.bitblaster;

import searchStructures.ObjectWithDistance;
import searchStructures.Quicksort;

import java.util.List;
import java.util.Objects;

/**
 * @author newrichard
 * 
 * 
 * @param <T>
 *            the type of the values
 */
public class BallExclusion<T> extends ExclusionZone<T> {

	private final RefPointSet<T> pointSet;
	private int ref;
	private double radius;

    public BallExclusion(RefPointSet<T> pointSet, int ref, double radius) {
		this.pointSet = pointSet;
		this.ref = ref;
		this.radius = radius;
	}

    public void setWitnesses(List<T> witnesses) {
        @SuppressWarnings("unchecked")
		ObjectWithDistance<Integer>[] owds = new ObjectWithDistance[witnesses.size()];
        for (int i = 0; i < witnesses.size(); i++) {
            double d1 = this.pointSet.extDist(witnesses.get(i), this.ref);
            owds[i] = new ObjectWithDistance<>(0, d1);
        }
        Quicksort.placeMedian(owds);
        this.radius = owds[owds.length / 2].getDistance();
    }


	/**
	 * @return the radius
	 */
	public double getRadius() {
		return this.radius;
	}

	/**
	 * @param radius the radius to set
	 */
	public void setRadius(double radius) {
		this.radius = radius;
	}

	/**
	 * @return true if the point is to the left of a defined partition
	 * 
	 *         this is a bit weird because of the single-distance calculation
	 *         optimisation...
	 */
	@Override
	public boolean isIn(double[] dists) {
		return dists[this.ref] < this.radius;
	}

	@Override
	public boolean mustBeIn(double[] dists, double t) {
		return dists[this.ref] < this.radius - t;
	}

	@Override
	public boolean mustBeOut(double[] dists, double t) {
		return dists[this.ref] >= this.radius + t;
	}
}
