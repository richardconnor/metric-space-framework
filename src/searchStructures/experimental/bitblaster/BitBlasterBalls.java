package searchStructures.experimental.bitblaster;

import java.util.ArrayList;
import java.util.BitSet;
import java.util.Collection;
import java.util.List;

import coreConcepts.CountedMetric;
import coreConcepts.Metric;
import eu.similarity.msc.data.cartesian.CartesianPoint;
import searchStructures.SearchIndex;
import testloads.TestContext;
import testloads.TestContext.Context;

public class BitBlasterBalls<T> extends SearchIndex<T> {

	protected static final double RADIUS_INCREMENT = 0;
	private static double MEAN_DIST = 0;
//	protected static double[] ball_radii = new double[] { MEAN_DIST - 2 * RADIUS_INCREMENT,
//			MEAN_DIST - RADIUS_INCREMENT, MEAN_DIST, MEAN_DIST - RADIUS_INCREMENT, MEAN_DIST - 2 * RADIUS_INCREMENT };
	protected static double[] ball_radii = new double[] { 0 };

	private List<T> refPoints;
	private List<ExclusionZone<T>> ezs;
	private RefPointSet<T> rps;
	private BitSet[] datarep;

	public BitBlasterBalls(List<T> data, List<T> refPoints, Metric<T> metric) {
		super(data, metric);
		this.refPoints = refPoints;
		this.ezs = new ArrayList<>();
		// TODO temp for balls only
		int noOfRefPoints = refPoints.size();

//		int nChoose2 = ((noOfRefPoints - 1) * noOfRefPoints) / 2;
		int noOfBitSets = (noOfRefPoints * ball_radii.length);
		this.datarep = new BitSet[noOfBitSets];

		// TODO generalise witness set maybe to creation parameter
		List<T> witnessSet = data.subList(0, 500);

		this.rps = new RefPointSet<>(this.refPoints, this.metric);

//		this.ezs.addAll(getSheetExclusions(witnessSet, this.rps, true, false, false));
		this.ezs.addAll(getBallExclusions(witnessSet, true));

		buildBitSetData();
	}

	private void buildBitSetData() {
		int dataSize = this.data.size();
		for (int i = 0; i < this.datarep.length; i++) {
			this.datarep[i] = new BitSet(dataSize);
		}
		for (int n = 0; n < dataSize; n++) {
			T p = this.data.get(n);
			double[] dists = this.rps.extDists(p);
			for (int x = 0; x < this.datarep.length; x++) {
				boolean isIn = this.ezs.get(x).isIn(dists);
				if (isIn) {
					this.datarep[x].set(n);
				}
			}
		}
	}

	private List<ExclusionZone<T>> getSheetExclusions(List<T> witnessSet, RefPointSet<T> rps, boolean balanced,
			boolean fourPoint, boolean rotationEnabled) {
		List<ExclusionZone<T>> res = new ArrayList<>();
		for (int i = 0; i < this.refPoints.size() - 1; i++) {
			for (int j = i + 1; j < this.refPoints.size(); j++) {
				if (fourPoint) {
					SheetExclusion4p<T> se = new SheetExclusion4p<>(rps, i, j);
					if (balanced) {
						se.setRotationEnabled(rotationEnabled);
						se.setWitnesses(witnessSet);
					}
					res.add(se);
				} else {
					SheetExclusion3p<T> se = new SheetExclusion3p<>(rps, i, j);
					if (balanced) {
						se.setWitnesses(witnessSet);
					}
					res.add(se);
				}
			}
		}
		return res;
	}

	private List<ExclusionZone<T>> getBallExclusions(List<T> witnessSet, boolean balanced) {
		/*
		 * one balanced ball per reference point
		 */
		List<ExclusionZone<T>> res = new ArrayList<>();
		for (int i = 0; i < this.refPoints.size(); i++) {
			BallExclusion<T> be = new BallExclusion<>(this.rps, i, 0);
			be.setWitnesses(witnessSet);
			res.add(be);
		}
		return res;
	}

	@SuppressWarnings("boxing")
	@Override
	public List<T> thresholdSearch(T query, double t) {
		List<T> res = new ArrayList<>();
		/*
		 * calculates distances from query to reference set, also adds results to res if
		 * distance is less than t
		 */
		double[] dists = this.rps.extDists(query, res, t);

		List<Integer> mustBeIn = new ArrayList<>();
		List<Integer> cantBeIn = new ArrayList<>();

		for (int i = 0; i < this.ezs.size(); i++) {
			ExclusionZone<T> ez = this.ezs.get(i);
			if (ez.mustBeIn(dists, t)) {
				mustBeIn.add(i);
			} else if (ez.mustBeOut(dists, t)) {
				cantBeIn.add(i);
			}
		}

		BitSet inclusions = doExclusions(mustBeIn, cantBeIn);

		if (inclusions == null) {
			for (T d : this.data) {
				if (this.metric.distance(query, d) <= t) {
					res.add(d);
				}
			}
		} else {
			filterContenders(this.data, t, this.metric, query, res, this.data.size(), inclusions);
		}
		return res;
	}

	@Override
	public String getShortName() {
		return "BB";
	}

	private BitSet doExclusions(List<Integer> mustBeIn, List<Integer> cantBeIn) {
		int dataSize = this.data.size();
		if (mustBeIn.size() != 0) {
			BitSet ands = getAndBitSets(mustBeIn, dataSize);
			if (cantBeIn.size() != 0) {
				/*
				 * hopefully the normal situation or we're in trouble!
				 */
				BitSet nots = getOrBitSets(cantBeIn, dataSize);
				nots.flip(0, dataSize);
				ands.and(nots);
				return ands;
			} else {
				// there are no cantBeIn partitions
				return ands;
			}
		} else {
			// there are no mustBeIn partitions
			if (cantBeIn.size() != 0) {
				BitSet nots = getOrBitSets(cantBeIn, dataSize);
				nots.flip(0, dataSize);
				return nots;
			} else {
				// there are no exclusions at all...
				return null;
			}
		}
	}

	@SuppressWarnings("boxing")
	private BitSet getAndBitSets(List<Integer> mustBeIn, int dataSize) {
		BitSet ands = null;
		if (mustBeIn.size() != 0) {
			ands = datarep[mustBeIn.get(0)].get(0, dataSize);
			for (int i = 1; i < mustBeIn.size(); i++) {
				ands.and(datarep[mustBeIn.get(i)]);
			}
		}
		return ands;
	}

	@SuppressWarnings("boxing")
	private BitSet getOrBitSets(List<Integer> cantBeIn, int dataSize) {
		BitSet nots = null;
		if (cantBeIn.size() != 0) {
			nots = datarep[cantBeIn.get(0)].get(0, dataSize);
			for (int i = 1; i < cantBeIn.size(); i++) {
				final BitSet nextNot = datarep[cantBeIn.get(i)];
				nots.or(nextNot);
			}
		}
		return nots;
	}

	static <T> void filterContenders(List<T> dat, double t, Metric<T> cm, T q, Collection<T> res, final int dataSize,
			BitSet results) {
		for (int i = results.nextSetBit(0); i >= 0; i = results.nextSetBit(i + 1)) {
			if (cm.distance(q, dat.get(i)) <= t) {
				res.add(dat.get(i));
			}
		}
	}

	public static void main(String[] args) throws Exception {

		Context context = Context.colors;

		boolean fourPoint = true;
		boolean balanced = false;
		boolean rotationEnabled = true;
		int noOfRefPoints = 500;

		TestContext tc = new TestContext(context);
		int querySize = (context == Context.colors || context == Context.nasa) ? tc.dataSize() / 10 : 1000;
		tc.setSizes(querySize, noOfRefPoints);
		List<CartesianPoint> dat = tc.getData();

		List<CartesianPoint> refs = tc.getRefPoints();
		double threshold = tc.getThresholds()[0];

		List<CartesianPoint> queries = tc.getQueries();

		CountedMetric<CartesianPoint> cm = new CountedMetric<>(tc.metric());
		BitBlasterBalls<CartesianPoint> bb = new BitBlasterBalls<>(dat, refs, cm);
		cm.reset();

		int results = 0;
		for (CartesianPoint q : queries) {
			List<CartesianPoint> res = bb.thresholdSearch(q, threshold);
			results += res.size();
		}
		System.out.println(results + "\t" + cm.reset() / querySize);
	}

}
