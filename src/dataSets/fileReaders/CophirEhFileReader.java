package dataSets.fileReaders;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;

import util.Range;
import dataPoints.cartesian_float.CartesianPointL1Norm;
import dataPoints.cartesian_float.CartesianPointL2Norm;
import eu.similarity.msc.data.cartesian.CartesianPoint;

public class CophirEhFileReader extends TreeMap<Integer, float[]> {

	// add integer for full data filename
	public static String inputRoot = "/Volumes/Data/cophir/data/zips/CoPhIR-";
	public static String ehRoot = "/Volumes/Data/cophir/data/eh80maps/";
	public static String mirFlickrEhRoot = "/Volumes/Data/mirflickr/features_edgehistogram/";

	public static List<CartesianPoint> getCartesianPoints(int fileId)
			throws IOException, ClassNotFoundException {
		List<CartesianPoint> res = new ArrayList<>();
		for (float[] fs : getPoints(fileId)) {
			res.add(new CartesianPoint(fs));
		}
		return res;
	}

	/**
	 * very temporary to allow reading of the MirFlickr data in this class
	 * intended for Cophir. However a more general solution is demanded, it
	 * doesn't need to be just for cophir!
	 * 
	 * @param fileId
	 * @return
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	public static Map<Integer, float[]> getPoints(String fileId)
			throws IOException, ClassNotFoundException {
		FileInputStream fis = new FileInputStream(ehRoot + fileId + ".dat");
		ObjectInputStream ois = new ObjectInputStream(fis);
		Map<Integer, float[]> ehs = (Map<Integer, float[]>) ois.readObject();
		ois.close();
		return ehs;
	}

	public static List<float[]> getPoints(int fileId) throws IOException,
			ClassNotFoundException {
		List<float[]> res = new ArrayList<>();
		FileInputStream fis = new FileInputStream(ehRoot + fileId + ".dat");
		ObjectInputStream ois = new ObjectInputStream(fis);
		Map<Integer, float[]> ehs = (Map<Integer, float[]>) ois.readObject();
		ois.close();
		for (float[] fs : ehs.values()) {
			res.add(fs);
		}
		return res;
	}

	public static List<CartesianPointL2Norm> getPointsNorm2(int fileId)
			throws IOException, ClassNotFoundException {
		List<CartesianPointL2Norm> res = new ArrayList<>();
		FileInputStream fis = new FileInputStream(ehRoot + fileId + ".dat");
		ObjectInputStream ois = new ObjectInputStream(fis);
		Map<Integer, float[]> ehs = (Map<Integer, float[]>) ois.readObject();
		ois.close();
		for (float[] fs : ehs.values()) {
			res.add(new CartesianPointL2Norm(fs));
		}
		return res;
	}

	public static List<CartesianPointL1Norm> getPointsNorm1(int fileId)
			throws IOException, ClassNotFoundException {
		List<CartesianPointL1Norm> res = new ArrayList<>();
		FileInputStream fis = new FileInputStream(ehRoot + fileId + ".dat");
		ObjectInputStream ois = new ObjectInputStream(fis);
		Map<Integer, float[]> ehs = (Map<Integer, float[]>) ois.readObject();
		ois.close();
		for (float[] fs : ehs.values()) {
			res.add(new CartesianPointL1Norm(fs));
		}
		return res;
	}

	public static void writeEhDataFile(int fileId)
			throws FileNotFoundException, IOException {
		Map<Integer, float[]> ehMap = new TreeMap<>();
		FileReader fr = new FileReader(inputRoot + fileId);
		LineNumberReader lr = new LineNumberReader(fr);
		String readLine = lr.readLine();
		int lines = 0;
		while (readLine != null) {
			Scanner s = new Scanner(readLine.replace(';', ' '));
			int id = s.nextInt();
			int dims = 80;
			float[] ehDat = new float[dims];
			for (int i = 0; i < dims; i++) {
				ehDat[i] = s.nextFloat();
			}
			ehMap.put(id, ehDat);
			readLine = lr.readLine();
			if (lines % 1000 == 0) {
				System.out.println(lines);
			}
			lines++;
		}
		fr.close();
		System.out.println(lines);

		FileOutputStream fos = new FileOutputStream(ehRoot + fileId + ".dat");
		ObjectOutputStream oos = new ObjectOutputStream(fos);
		oos.writeObject(ehMap);
		oos.close();
	}

	private static void writeMirFlickrEhDataFile()
			throws FileNotFoundException, IOException {
		Map<Integer, float[]> ehMap = new TreeMap<>();

		for (int mfFolder = 0; mfFolder < 100; mfFolder++) {
			for (int mfFile = 0; mfFile < 10000; mfFile++) {
				int fileId = mfFolder * 10000 + mfFile;
				FileReader fr = new FileReader(mirFlickrEhRoot + mfFolder + "/"
						+ fileId + ".txt");

				LineNumberReader lr = new LineNumberReader(fr);

				int dims = 80;
				float[] ehDat = new float[dims];
				for (int i = 0; i < dims; i++) {
					String readLine = lr.readLine();
					ehDat[i] = Float.parseFloat(readLine);
				}
				fr.close();

				ehMap.put(fileId, ehDat);
			}
		}

		FileOutputStream fos = new FileOutputStream(ehRoot + "MF" + ".dat");
		ObjectOutputStream oos = new ObjectOutputStream(fos);
		oos.writeObject(ehMap);
		oos.close();
	}

	public static void main(String[] a) throws FileNotFoundException,
			IOException {
		for (int i : Range.range(33, 43)) {
			writeEhDataFile(i);
		}
		// writeMirFlickrEhDataFile();
	}
}
