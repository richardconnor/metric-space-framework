package extreme_fourpoint;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import searchStructures.VPTree;
import util.Range;
import coreConcepts.Metric;
import dataPoints.cartesian.Euclidean;
import dataSets.fileReaders.CartesianPointFileReader;
import eu.similarity.msc.data.cartesian.CartesianPoint;

public class ExpContext {
	public static String rootDir = "/Volumes/Data/superExtreme/testData/";
	private static Random rand = new Random(0);

	public static double[] colorsThresholds = { 0.052, 0.083, 0.131 };
	public static double[] nasaThresholds = { 0.12, 0.285, 0.53 };
	private static double euc10threshold = 0.235107454;

	enum Data {
		colors, nasa, alexnet, euc10, yfcc
	};

	private int noOfRefPoints;
	private int noOfWitnessData;
	Data dataset;
	double queryThreshold;
	double createThreshold;
	double filterThreshold;
	boolean kNN;
	boolean fft;
	int k;
	int t;
	int noOfResultsAtThreshold;
	Metric<CartesianPoint> metric;

	ExpContext(Data data, int noOfRefPoints) {
		this.noOfRefPoints = noOfRefPoints;
		this.dataset = data;
		this.noOfWitnessData = 1000;
		this.queryThreshold = colorsThresholds[0];
		this.kNN = true;
		this.k = 3;
		this.fft = false;
		this.metric = new Euclidean<>();
		this.noOfResultsAtThreshold = 0;
		this.filterThreshold = colorsThresholds[0];
	}

	public String getSurDataFileName(boolean serial) {
		final String fname = rootDir + dataset + "/sur_" + this.noOfRefPoints
				+ "_" + this.noOfWitnessData + (this.fft ? "_fft" : "") + "_"
				+ this.metric.getMetricName()
				+ (this.kNN ? "_nn" + this.k : "_t" + this.t)
				+ (serial ? "s" : "") + ".dat";
		return fname;
	}

	public int calculateResultSetSize() throws Exception {
		List<CartesianPoint> queries = getQueries();
		List<CartesianPoint> dat = getBigData();

		VPTree<CartesianPoint> vpt = new VPTree<>(dat, this.metric);
		int res = 0;
		for (CartesianPoint q : queries) {
			res += vpt.thresholdSearch(q, this.queryThreshold).size();
		}
		return res;
	}

	public List<CartesianPoint> getBigData() throws Exception {
		return new CartesianPointFileReader(rootDir + this.dataset
				+ "/bigdata.dat", false);
	}

	public List<CartesianPoint> getQueries() throws Exception {
		return new CartesianPointFileReader(rootDir + this.dataset
				+ "/queries.dat", false);
	}

	public List<CartesianPoint> getTestData() throws Exception {
		return new CartesianPointFileReader(rootDir + this.dataset
				+ "/testdata.dat", false);
	}

	public List<CartesianPoint> getPivots() throws Exception {
		return new CartesianPointFileReader(rootDir + this.dataset
				+ "/pivots.dat", false).subList(0, this.noOfRefPoints);
	}

	public List<CartesianPoint> getWitnesses() throws Exception {
		return new CartesianPointFileReader(rootDir + this.dataset
				+ "/witness.dat", false).subList(0, this.noOfWitnessData);
	}

	public void generatePivotTableData() throws Exception {

		List<CartesianPoint> pivs = this.getPivots();

		Metric<CartesianPoint> euc = new Euclidean<>();

		FourPointPivotsChooser<CartesianPoint> pc = new FourPointPivotsChooser<>(
				pivs, euc, 0, false, 0);

		pc.writePivotDistanceTable(this.rootDir + this.dataset + "/pivotDists"
				+ this.noOfRefPoints + "_" + "_euc" + ".dat");

	}

	public void createTestData(List<CartesianPoint> data, String dataName)
			throws Exception, FileNotFoundException {

		File f = new File(rootDir + dataName);
		if (!f.exists()) {
			f.mkdir();
		}

		printDataFile(data, dataName, "/pivots", 3000);
		printDataFile(data, dataName, "/witness", 5000);
		printDataFile(data, dataName, "/testdata", 1000);
		printDataFile(data, dataName, "/queries", 1000);
		printDataFile(data, dataName, "/bigdata", 10000);
	}

	private static void printDataFile(List<CartesianPoint> data,
			final String dataSet, final String fileType, final int number)
			throws FileNotFoundException {

		PrintWriter pw = new PrintWriter(rootDir + dataSet + fileType + ".dat");

		for (int i : Range.range(0, number)) {
			CartesianPoint p = data.remove(rand.nextInt(data.size()));
			for (double d : p.getPoint()) {
				pw.print(d + "\t");
			}
			pw.println();
		}
		pw.close();
	}

	public void setUpData() throws Exception, FileNotFoundException {
		// TestLoad tl = new TestLoad(SisapFile.colors);
		// List<CartesianPoint> data = tl.getData();
		// createTestData(data, "colors");
		//
		// TestLoad tl = new TestLoad(SisapFile.nasa);
		// List<CartesianPoint> data = tl.getData();
		// createTestData(data, "nasa");
		//
		// Map<Integer, float[]> map = MFExperiments.getRawFile(0);
		// List<CartesianPoint> data = new ArrayList<>();
		// for (float[] fs : map.values()) {
		// CartesianPoint cp = new CartesianPoint(fs);
		// data.add(cp);
		// }

		FileInputStream fis = new FileInputStream(
				"/docsNotOnIcloud/Research/yfcc/data1_20k.obj");
		ObjectInputStream ois = new ObjectInputStream(fis);
		float[][] dat = (float[][]) ois.readObject();
		ois.close();
		List<CartesianPoint> data = new ArrayList<>();
		for (float[] floats : dat) {
			CartesianPoint cp = new CartesianPoint(floats);
			data.add(cp);
		}
		createTestData(data, "yfcc");

		// CartesianPointGenerator cg = new CartesianPointGenerator(10, false);
		// List<CartesianPoint> data = new ArrayList<>();
		// for (int i : Range.range(0, 20000)) {
		// data.add(cg.next());
		// }
		// createTestData(data, "euc10");
	}

	public float[][] getPivotDistances() throws ClassNotFoundException,
			IOException {

		String pivotDistTable = rootDir + this.dataset + "/pivotDists"
				+ this.noOfRefPoints + "_" + "_euc" + ".dat";
		FileInputStream fis1 = new FileInputStream(pivotDistTable);
		ObjectInputStream ois1 = new ObjectInputStream(fis1);
		final float[][] dists = (float[][]) ois1.readObject();
		ois1.close();
		return dists;
	}

	public static ExpContext easyNasaEc(int refPoints) {
		ExpContext ec = new ExpContext(Data.nasa, refPoints);
		ec.queryThreshold = 0.12;
		ec.kNN = true;
		ec.k = 3;
		ec.fft = false;
		ec.metric = new Euclidean<>();
		ec.noOfResultsAtThreshold = 1308;
		ec.filterThreshold = 0.12;
		return ec;
	}

	public static ExpContext easyColorsEc(int refPoints) {
		final ExpContext colors3NN = new ExpContext(ExpContext.Data.colors,
				refPoints);
		colors3NN.queryThreshold = colorsThresholds[0];
		colors3NN.kNN = true;
		colors3NN.k = 3;
		colors3NN.fft = false;
		colors3NN.metric = new Euclidean<>();
		colors3NN.noOfResultsAtThreshold = 1858;
		colors3NN.filterThreshold = colorsThresholds[0];
		return colors3NN;
	}

	public static ExpContext easyYfcc() {
		final ExpContext colors3NN = new ExpContext(ExpContext.Data.yfcc, 128);
		colors3NN.queryThreshold = 0.75;
		colors3NN.kNN = true;
		colors3NN.k = 3;
		colors3NN.fft = false;
		colors3NN.metric = new Euclidean<>();
		colors3NN.noOfResultsAtThreshold = 117;
		colors3NN.filterThreshold = 0.75;
		return colors3NN;
	}

	public void setThresholds(double d) {
		this.queryThreshold = d;
		this.filterThreshold = d;
	}
}
