package extreme_fourpoint;

import java.util.ArrayList;
import java.util.List;

import coreConcepts.Metric;
import searchStructures.ObjectWithDistance;
import searchStructures.Quicksort;
import sisap_2017_experiments.NdimSimplex;

public class LowDistLowAltPivotsChooser<T> extends GeometricPivotsChooser<T> {

	LowDistLowAltPivotsChooser(List<T> refPoints, Metric<T> metric) {
		super(refPoints, metric);
	}

	@Override
	protected void assign1stReferenceToData(List<T> allData) {
		for (T datum : allData) {
			DataPoint<T> dp = this.results.get(datum);

			for (int i = 0; i < this.refPoints.size(); i++) {
				double bestDist = 0;

				double dist = this.metric
						.distance(datum, this.refPoints.get(i));
				if (dist < bestDist) {
					bestDist = dist;
					dp.refs[1] = i;
					dp.dists[1] = dist;
				}
			}

		}
	}

	@Override
	protected void assign2ndReferenceToData(List<T> allData) {
		for (T datum : allData) {
			DataPoint<T> dp = this.results.get(datum);

			int p1 = dp.refs[0];
			double d1 = dp.dists[0];

			for (int i = 0; i < this.refPoints.size(); i++) {
				double bestAlt = Double.MAX_VALUE;
				if (i != p1) {
					double d2 = this.metric.distance(datum,
							this.refPoints.get(i));
					NdimSimplex<T> sim = this.pivSimplexTable[p1][i];
					double[] dists = { d1, d2 };
					double[] ap = sim.getApex(dists);
					if (ap[1] < bestAlt) {
						bestAlt = ap[1];
						dp.refs[1] = i;
						dp.dists[1] = d2;
					}
				}
			}
		}

	}

	private void assignReferencesToData(List<T> allData, int chunkSize,
			int missing, boolean first) {
		List<T> unplaced = new ArrayList<>();
		int iteration = first ? 0 : 1;
		for (int index = 0; index < this.refPoints.size() - 1; index++) {
			int i = first ? index : this.refPoints.size() - (index + 1);
			// System.out.println("doing pivot " + i);
			final int remainingDataSize = allData.size();
			int chsize = index < missing ? chunkSize + 1 : chunkSize;

			T pivot = this.refPoints.get(i);
			ObjectWithDistance<T>[] owds = new ObjectWithDistance[remainingDataSize];

			for (int notI = 0; notI < remainingDataSize; notI++) {
				final T point = allData.get(notI);
				owds[notI] = new ObjectWithDistance<>(point,
						this.metric.distance(pivot, point));
			}

			Quicksort.placeOrdinal(owds, chsize);

			for (int x = 0; x < chsize; x++) {
				T next = owds[x].getValue();
				DataPoint<T> dp = this.results.get(next);
				dp.refs[iteration] = i;
				dp.dists[iteration] = owds[x].getDistance();

				allData.remove(next);
				if (!first && dp.refs[0] == dp.refs[1]) {
					unplaced.add(next);
				}

			}
			// System.out.println("data size " + allData.size());
		}
		final int lastRefIndex = first ? refPoints.size() - 1 : 0;
		T lastRef = refPoints.get(lastRefIndex);
		// System.out.println("doing pivot " + lastRefIndex);
		for (T residual : allData) {
			DataPoint<T> dp = this.results.get(residual);
			dp.refs[iteration] = lastRefIndex;
			dp.dists[iteration] = this.metric.distance(residual,
					this.refPoints.get(lastRefIndex));
			if (!first && dp.refs[0] == dp.refs[1]) {
				unplaced.add(residual);
			}
		}
		for (T duplicate : unplaced) {
			DataPoint<T> dp = this.results.get(duplicate);
			// dp.refs[1] = (dp.refs[0] + 1) % this.refPoints.size();
			// dp.dists[1] = metric.distance(duplicate,
			// this.refPoints.get(dp.refs[1]));
			double closest = Double.MAX_VALUE;
			for (int refPointIndex = 0; refPointIndex < this.refPoints.size(); refPointIndex++) {
				if (refPointIndex != dp.refs[0]) {
					double dist = this.metric.distance(
							this.refPoints.get(refPointIndex), duplicate);
					if (dist < closest) {
						dp.refs[1] = refPointIndex;
						dp.dists[1] = dist;
						closest = dist;
					}
				}
			}
		}
	}

	@Override
	public DataPoint<T> getDataPoint(T datum) {
		// fill in apex values here
		final DataPoint<T> res = this.results.get(datum);
		if (res.refs[0] > res.refs[1]) {
			int temp = res.refs[0];
			res.refs[0] = res.refs[1];
			res.refs[1] = temp;
			double temp1 = res.dists[0];
			res.dists[0] = res.dists[1];
			res.dists[1] = temp1;
		}
		NdimSimplex<T> sim = this.pivSimplexTable[res.refs[0]][res.refs[1]];
		res.apex = sim.getApex(res.dists);
		return res;
	}

}
