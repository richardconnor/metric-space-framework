package extreme_fourpoint;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.List;

import coreConcepts.Metric;

public class FourPointData<T> {

	StoredExclusions se;
	float[][] pivDists;
	List<T> pivs;
	Metric<T> metric;
	List<T> data;

	FourPointData(String filename, Metric<T> metric, List<T> data,
			List<T> pivots) throws IOException, ClassNotFoundException {
		FileInputStream fis2 = new FileInputStream(filename);
		ObjectInputStream ois2 = new ObjectInputStream(fis2);
		this.se = (StoredExclusions) ois2.readObject();
		fis2.close();
		this.metric = metric;
		this.pivs = pivots;
		this.data = data;
		this.pivDists = new float[pivots.size()][pivots.size()];
		for (int i = 0; i < pivots.size() - 1; i++) {
			for (int j = i + 1; j < pivots.size(); j++) {
				pivDists[i][j] = (float) metric.distance(pivots.get(i),
						pivots.get(j));
			}
		}
	}

	public List<T> query(T query, double threshold) {

		int dataSize = se.dist1s.length;

		float[] dists = new float[pivs.size()];
		for (int i = 0; i < pivs.size(); i++) {
			dists[i] = (float) metric.distance(query, pivs.get(i));
		}
		List<T> res = new ArrayList<>();
		for (int i = 0; i < dataSize; i++) {
			float[] apex1 = { se.dist1s[i], se.dist2s[i] };
			
			float pDist = pivDists[se.piv1s[i]][se.piv2s[i]];
			float d1 = dists[se.piv1s[i]];
			float d2 = dists[se.piv2s[i]];

			float x_offset = (d1 * d1 + pDist * pDist - d2 * d2) / (2 * pDist);
			float y_offset = (float) Math.sqrt(d1 * d1 - x_offset * x_offset);
			float[] apex2 = { x_offset, y_offset };
			double lwb = l2_2dim(apex1, apex2);
			if (lwb < threshold) {
				final double trueD = metric.distance(query, data.get(i));
				if (trueD <= threshold) {
					res.add(data.get(i));
				}
			}
		}
		return res;
	}

	public static float[] getApex(float pDist, float d1, float d2) {
		float x_offset = (d1 * d1 + pDist * pDist - d2 * d2) / (2 * pDist);
		float y_offset = (float) Math.sqrt(d1 * d1 - x_offset * x_offset);
		float[] apex = { x_offset, y_offset };
		return apex;
	}

	public static float l2_2dim(float[] a, float[] b) {
		float a0 = a[0] - b[0];
		float a1 = a[1] - b[1];

		return (float) Math.sqrt(a0 * a0 + a1 * a1);
	}
	
	public int size(){
		return this.se.size();
	}

}
