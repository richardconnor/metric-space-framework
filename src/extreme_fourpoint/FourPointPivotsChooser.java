package extreme_fourpoint;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import searchStructures.ObjectWithDistance;
import searchStructures.Quicksort;
import searchStructures.VPTree;
import sisap_2017_experiments.NdimSimplex;
import coreConcepts.Metric;

public class FourPointPivotsChooser<T> extends RefPointsSelector<T> {

	private double SEARCH_THRESHOLD;

	private class Pivot {

		int pivotId;
		List<double[]> sampleData;
		VPTree<double[]> sampleIndex;
		private NdimSimplex<T> simp;
		int pivId1;
		int pivId2;

		int successes;
		int failures;

		public Pivot(int id1, int id2, int pivotId) {
			this.pivId1 = id1;
			this.pivId2 = id2;
			this.pivotId = pivotId;
			this.successes = 0;
			this.failures = 0;
			List<T> pivs = new ArrayList<>();
			pivs.add(refPoints.get(id1));
			pivs.add(refPoints.get(id2));
			this.simp = new NdimSimplex<>(metric, pivs);
		}

		@SuppressWarnings("synthetic-access")
		public int density(T point, double[] pointToPivDists) {

			// find coordinates in plot
			double d1 = pointToPivDists[this.pivId1];
			double d2 = pointToPivDists[this.pivId2];
			double[] ds = { d1, d2 };
			double[] ap = this.simp.getApex(ds);

			List<double[]> nn = this.sampleIndex.thresholdSearch(ap,
					SEARCH_THRESHOLD);

			return nn.size();
		}

		public double nnRadius(T point, double[] pointToPivDists, int nn) {

			// find coordinates in plot
			double d1 = pointToPivDists[this.pivId1];
			double d2 = pointToPivDists[this.pivId2];
			double[] ds = { d1, d2 };
			double[] ap = this.simp.getApex(ds);

			List<Integer> nns = this.sampleIndex.nearestNeighbour(ap, nn);
			double maxRad = 0;
			for (int i : nns) {
				double d = ExtremeFourPoint.getL2().distance(
						this.sampleData.get(i), ap);
				maxRad = Math.max(maxRad, d);
			}

			return maxRad;
		}

		public double badness(T point, double[] pointToPivDists) {

			// find coordinates in plot
			double d1 = pointToPivDists[this.pivId1];
			double d2 = pointToPivDists[this.pivId2];
			double[] ds = { d1, d2 };
			double[] ap = this.simp.getApex(ds);

			List<double[]> nn = this.sampleIndex.thresholdSearch(ap,
					SEARCH_THRESHOLD);

			return nn.size();
		}

		public double[] getApex(double[] dists) {
			return this.simp.getApex(dists);
		}

		@SuppressWarnings("synthetic-access")
		public void setSample(List<T> sample, double[][] distTable) {
			this.sampleData = new ArrayList<>();
			for (int samp = 0; samp < sample.size(); samp++) {
				double d1 = distTable[samp][this.pivId1];
				double d2 = distTable[samp][this.pivId2];
				double[] ds = { d1, d2 };
				double[] newp = this.simp.getApex(ds);
				this.sampleData.add(newp);
			}

			this.sampleIndex = new VPTree<>(this.sampleData,
					ExtremeFourPoint.getL2());
		}
	}

	List<Pivot> pivots;
	private boolean SEARCH_BY_THRESHOLD = false;
	private int K_FOR_KNN;

	public FourPointPivotsChooser(List<T> refPoints, Metric<T> metric,
			double threshold, boolean searchByThreshold, int kForkNN) {
		super(refPoints, metric);
		pivots = new ArrayList<>();
		this.SEARCH_THRESHOLD = threshold;
		if (searchByThreshold) {
			this.SEARCH_BY_THRESHOLD = true;
		}
		this.K_FOR_KNN = kForkNN;
	}

	@Override
	public void setWitnesses(List<T> witnesses) {

		double[][] sampleToPivDists = new double[witnesses.size()][refPoints
				.size()];

		for (int samp = 0; samp < witnesses.size(); samp++) {
			for (int piv = 0; piv < refPoints.size(); piv++) {
				sampleToPivDists[samp][piv] = metric.distance(
						witnesses.get(samp), refPoints.get(piv));
			}
		}

		for (int i = 0; i < refPoints.size() - 1; i++) {
			for (int j = i + 1; j < refPoints.size(); j++) {
				final Pivot pivot = new Pivot(i, j, pivots.size());
				pivot.setSample(witnesses, sampleToPivDists);
				pivots.add(pivot);
			}
		}
	}

	@Override
	public DataPoint<T> getDataPoint(T datum) {
		DataPoint<T> result = new DataPoint<>();

		result.datum = datum;

		double[] refDists = getRefDists(datum);

		Pivot p = getBestPivot(datum, refDists, result, SEARCH_BY_THRESHOLD);

		result.pivotId = p.pivotId;

		int[] sd = { p.pivId1, p.pivId2 };
		result.refs = sd;

		double[] dds = { refDists[p.pivId1], refDists[p.pivId2] };
		result.dists = dds;
		result.apex = p.getApex(dds);

		return result;
	}

	public double[] scorePivotPairs(List<T> sample) {

		double[] res = new double[pivots.size()];
		System.out.println("calculating badness for...");
		int sampNo = 0;
		for (T samp : sample) {
			System.out.println("sample " + sampNo++);
			double[] ds = getRefDists(samp);
			int ptr = 0;
			for (Pivot p : this.pivots) {
				double x = p.badness(samp, ds);

				res[ptr++] += x;
			}
		}
		ObjectWithDistance<Pivot>[] owds = new ObjectWithDistance[pivots.size()];
		for (int i = 0; i < pivots.size(); i++) {
			owds[i] = new ObjectWithDistance<>(pivots.get(i), res[i]);
		}
		Quicksort.sort(owds);
		for (int i = pivots.size() / 2; i < pivots.size(); i++) {
			pivots.remove(owds[i].getValue());
		}
		int ptr = 0;
		for (Pivot p : pivots) {
			p.pivotId = ptr++;
		}
		return res;
	}

	public double[] getRefDists(T datum) {
		double[] pointToPivDists = new double[refPoints.size()];
		for (int i = 0; i < refPoints.size(); i++) {
			pointToPivDists[i] = metric.distance(datum, refPoints.get(i));
		}
		return pointToPivDists;
	}

	/**
	 * @param datum
	 * @param refDists
	 * @param dp
	 *            horrible hack, passing this in just to get side-effect to
	 *            track density outside the context
	 * @return
	 */
	private Pivot getBestPivot(T datum, double[] refDists, DataPoint<T> dp,
			boolean threshold) {

		double bestDensity = Double.MAX_VALUE;
		double bestRadius = 0;
		Pivot bestPivot = null;
		for (Pivot piv : this.pivots) {
			double d = threshold ? piv.density(datum, refDists) : piv.nnRadius(
					datum, refDists, K_FOR_KNN);
			if (threshold ? d <= bestDensity : d >= bestRadius) {
				bestDensity = d;
				bestRadius = d;
				bestPivot = piv;
				dp.density = (int) d;
				dp.radius = d;
			}
		}

		return bestPivot;
	}

	public double[] pivotApex(int pivotId, double[] dists) {
		return this.pivots.get(pivotId).getApex(dists);
	}

	public void writePivotDistanceTable(String filename) throws IOException {
		float[][] dists = new float[this.refPoints.size()][this.refPoints
				.size()];
		for (int i = 0; i < this.refPoints.size() - 1; i++) {
			for (int j = i + 1; j < this.refPoints.size(); j++) {
				double d = this.metric.distance(this.refPoints.get(i),
						this.refPoints.get(j));
				dists[i][j] = (float) d;
			}
		}
		OutputStream fw = new FileOutputStream(filename);
		ObjectOutputStream oos = new ObjectOutputStream(fw);
		oos.writeObject(dists);
		oos.close();
	}
}
