package extreme_fourpoint;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.util.List;

import coreConcepts.Metric;
import eu.similarity.msc.data.cartesian.CartesianPoint;

public abstract class RefPointsSelector<T> {

	public static class DataPoint<S> {
		public S datum;
		public int[] refs;
		public double[] dists;
		public double[] apex;
		public int pivotId;
		public int density;
		public double radius;
	}

	protected List<T> refPoints;
	protected Metric<T> metric;

	/**
	 * creates some reference point based structure with exclusion potential
	 * 
	 * @param refPoints
	 * @param metric
	 */
	RefPointsSelector(List<T> refPoints, Metric<T> metric) {
		this.refPoints = refPoints;
		this.metric = metric;
	}

	/**
	 * uses a set of representative data to create useful exclusion structures
	 * internally
	 * 
	 * @param witnesses
	 */
	public abstract void setWitnesses(List<T> witnesses);

	/**
	 * 
	 * @param datum
	 * @return the ids of the original reference points used to create a chosen
	 *         Exclusion structure
	 */
	public abstract DataPoint<T> getDataPoint(T datum);

	public void writeDataToFile(List<T> dat, String filename)
			throws IOException {

		StoredExclusions se = new StoredExclusions();

		int size = dat.size();
		se.dist1s = new float[size];
		se.dist2s = new float[size];
		se.piv1s = new short[size];
		se.piv2s = new short[size];
		for (int i = 0; i < dat.size(); i++) {
			if (i % 100 == 0) {
				// System.out.println("doing " + i);
			}
			T d = dat.get(i);
			DataPoint<T> x = this.getDataPoint(d);
			se.dist1s[i] = (float) x.apex[0];
			se.dist2s[i] = (float) x.apex[1];
			se.piv1s[i] = (short) x.refs[0];
			se.piv2s[i] = (short) x.refs[1];
		}
		OutputStream fw = new FileOutputStream(filename);
		ObjectOutputStream oos = new ObjectOutputStream(fw);
		oos.writeObject(se);
		oos.close();
	}
}
