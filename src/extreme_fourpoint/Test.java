package extreme_fourpoint;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import searchStructures.VPTree;
import testloads.TestContext;
import testloads.TestContext.Context;
import coreConcepts.CountedMetric;
import coreConcepts.Metric;
import eu.similarity.msc.data.cartesian.CartesianPoint;

public class Test {

	private static String testPath = "/Volumes/Data/superExtreme/full_sets/";

	public static void main(String[] args) throws Exception {
		Context[] cs = { Context.nasa, Context.colors };

		for (Context c : cs) {
			final Context cont = c;

			System.out.println("lowHighDist selected pivots, SISAP " + cont);
			int[] noOfRefs = { 10, 50, 150, 500 };
			// int[] noOfRefs = { 1000 };
			System.out
					.println("pivots\tbuild time\tt0excl\tt0time\tt1excl\tt1time\tt2excl\tt2time");
			for (int i : noOfRefs) {
				System.out.print(i);
				String filename = "sisap_geom.tmp";

				TestContext tc = new TestContext(cont);
				tc.setSizes(tc.dataSize() / 10, 1000);
				List<CartesianPoint> pivots = getPivots(i, tc.getRefPoints());
				List<CartesianPoint> data = tc.getData();
				// List<CartesianPoint> witnesses = origDat.subList(0, kWits *
				// 1000);

				// System.out.println(queries.size());
				// System.out.println(data.size());

				long t0 = System.currentTimeMillis();
				buildTestData(tc.metric(), data, pivots, testPath + filename);
				long t1 = System.currentTimeMillis();
				System.out.print("\t" + (t1 - t0) / (float) data.size());
				// System.out.println(testPath + filename);

				// buildReferenceData(tc, data, witnesses, testPath + filename);
				// ratify(data, queries, new CountedMetric<>(tc.metric()),
				// tc.getThreshold());
				double[] thresholds = tc.getThresholds();
				List<CartesianPoint> queries = tc.getQueries();
				for (double t : thresholds) {
					queryData(tc.metric(), queries, pivots, data, testPath
							+ filename, t);
				}
				System.out.println();
			}
		}

	}

	private static List<CartesianPoint> getPivots(int noOfRefs,
			List<CartesianPoint> refPoints) {
		Random r = new Random();
		List<CartesianPoint> res = new ArrayList<>();
		for (int x = 0; x < noOfRefs; x++) {
			res.add(refPoints.remove(r.nextInt(refPoints.size())));
		}
		return res;
	}

	private static <T> void queryData(Metric<T> metric, List<T> queries,
			List<T> pivots, List<T> data, String filename, double threshold)
			throws Exception {

		CountedMetric<T> cm = new CountedMetric<>(metric);

		FourPointData<T> fpd = new FourPointData<>(filename, cm, data, pivots);

		int totRes = 0;
		cm.reset();
		long t0 = System.currentTimeMillis();

		for (T q : queries) {
			List<T> res = fpd.query(q, threshold);
			totRes += res.size();
		}

		long t1 = System.currentTimeMillis();
		// System.out.println(filename);
		// System.out.println("results: " + totRes);
		int qTimesDat = queries.size() * fpd.size();
		final int noOfDistCalcs = cm.reset();
		// System.out.println("calcs: " + noOfDistCalcs);
		System.out.print("\t"
				+ (1 - (noOfDistCalcs - pivots.size()) / (float) qTimesDat));
		System.out.print("\t" + (t1 - t0) / (float) queries.size());
	}

	private static void buildReferenceData(TestContext tc,
			List<CartesianPoint> data, List<CartesianPoint> witnesses,
			String filename) throws IOException {

		RefPointsSelector<CartesianPoint> pivotsChooser = new FourPointPivotsChooser<>(
				tc.getRefPoints(), tc.metric(), 0.02, false, 3);
		pivotsChooser.setWitnesses(witnesses);

		pivotsChooser.writeDataToFile(data, filename);
	}

	private static <T> void buildTestData(Metric<T> metric, List<T> data,
			List<T> pivots, String filename) throws IOException {
		//
		// RefPointsSelector<CartesianPoint> pivotsChooser = new
		// LowDistLowAltPivotsChooser<>(
		// tc.getRefPoints(), tc.metric());

		RefPointsSelector<T> pivotsChooser = new DistPivotsChooser<>(pivots,
				metric);

		pivotsChooser.setWitnesses(data);

		pivotsChooser.writeDataToFile(data, filename);
	}

	private static void ratify(List<CartesianPoint> data,
			List<CartesianPoint> queries, CountedMetric<CartesianPoint> metric,
			double threshold) {

		VPTree<CartesianPoint> vpt = new VPTree<>(data, metric);
		long t0 = System.currentTimeMillis();
		int res = 0;
		metric.reset();
		for (CartesianPoint q : queries) {
			List<CartesianPoint> r = vpt.thresholdSearch(q, threshold);
			res += r.size();
		}
		long t1 = System.currentTimeMillis();
		System.out.println("results: " + res);
		System.out.println("calcs: " + metric.reset());
		System.out.println("time:" + (t1 - t0));
	}
}
