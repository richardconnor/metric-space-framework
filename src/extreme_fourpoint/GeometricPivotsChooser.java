package extreme_fourpoint;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import sisap_2017_experiments.NdimSimplex;
import coreConcepts.Metric;

public abstract class GeometricPivotsChooser<T> extends RefPointsSelector<T> {

	protected Map<T, DataPoint<T>> results;
	protected NdimSimplex<T>[][] pivSimplexTable;
	private static String temp_file_name = "/Volumes/Data/superExtreme/testData/XXX.xxx";

	GeometricPivotsChooser(List<T> refPoints, Metric<T> metric) {
		super(refPoints, metric);
		this.results = new HashMap<>();
		this.pivSimplexTable = new NdimSimplex[refPoints.size()][refPoints
				.size()];
		for (int i = 0; i < refPoints.size() - 1; i++) {
			for (int j = i + 1; j < refPoints.size(); j++) {
				List<T> refPair = new ArrayList<>();
				refPair.add(refPoints.get(i));
				refPair.add(refPoints.get(j));
				this.pivSimplexTable[i][j] = new NdimSimplex<>(metric, refPair);
				this.pivSimplexTable[j][i] = new NdimSimplex<>(metric, refPair);
			}
		}
	}

	@Override
	public void setWitnesses(List<T> witnesses) {

		Random r = new Random();

		for (T dat : witnesses) {
			DataPoint<T> dp = new DataPoint<>();
			dp.datum = dat;
			dp.refs = new int[2];
			dp.refs[0] = r.nextInt(this.refPoints.size());
			dp.refs[1] = r.nextInt(this.refPoints.size());
			while (dp.refs[1] == dp.refs[0]) {
				dp.refs[1] = r.nextInt(this.refPoints.size());
			}

			dp.dists = new double[2];
			dp.dists[0] = metric.distance(dat, this.refPoints.get(dp.refs[0]));
			dp.dists[1] = metric.distance(dat, this.refPoints.get(dp.refs[1]));
			this.results.put(dat, dp);
		}

		assign1stReferenceToData(witnesses);

		assign2ndReferenceToData(witnesses);
	}

	protected abstract void assign1stReferenceToData(List<T> allData);

	protected abstract void assign2ndReferenceToData(List<T> allData);

	@Override
	public DataPoint<T> getDataPoint(T datum) {
		// fill in apex values here
		final DataPoint<T> res = this.results.get(datum);
		if (res.refs[0] > res.refs[1]) {
			int temp = res.refs[0];
			res.refs[0] = res.refs[1];
			res.refs[1] = temp;
			double temp1 = res.dists[0];
			res.dists[0] = res.dists[1];
			res.dists[1] = temp1;
		}
		NdimSimplex<T> sim = this.pivSimplexTable[res.refs[0]][res.refs[1]];
		try {
			res.apex = sim.getApex(res.dists);
		} catch (Throwable t) {
			throw new RuntimeException(res == null ? "res is null"
					: "sim is null:" + res.refs[0] + ":" + res.refs[1]);
		}
		return res;
	}

	public static double l2_2dim(float[] a, float[] b) {
		float a0 = a[0] - b[0];
		float a1 = a[1] - b[1];

		return Math.sqrt(a0 * a0 + a1 * a1);
	}

}
