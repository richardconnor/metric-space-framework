package extreme_fourpoint;

import java.io.Serializable;

public class StoredExclusions implements Serializable {
	short[] piv1s;
	short[] piv2s;
	float[] dist1s;
	float[] dist2s;
	
	public int size(){
		return this.piv1s.length;
	}
}
