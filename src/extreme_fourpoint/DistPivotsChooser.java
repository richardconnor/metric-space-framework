package extreme_fourpoint;

import java.util.ArrayList;
import java.util.List;

import searchStructures.ObjectWithDistance;
import searchStructures.Quicksort;
import sisap_2017_experiments.NdimSimplex;
import coreConcepts.Metric;

public class DistPivotsChooser<T> extends GeometricPivotsChooser<T> {

	DistPivotsChooser(List<T> refPoints, Metric<T> metric) {
		super(refPoints, metric);
	}

	@Override
	protected void assign1stReferenceToData(List<T> witnesses) {
		// lowAltitudeSecondPass(witnesses, 0);
		lowDistributionByRefPoint(witnesses, 0);
	}

	@Override
	protected void assign2ndReferenceToData(List<T> witnesses) {
		// lowAltitudeSecondPass(witnesses, 1);
		lowAltitude(witnesses, 1, 0);
		// removeDuplicates(witnesses);
	}

	private void lowDistributionByRefPoint(List<T> witnesses, int iteration) {

		List<T> allData = new ArrayList<>();
		allData.addAll(witnesses);
		int chunkSize = witnesses.size() / this.refPoints.size();

		// System.out.println("chunk size:" + chunkSize);
		int missing = witnesses.size() % this.refPoints.size();

		// TODO Auto-generated method stub
		List<T> unplaced = new ArrayList<>();
		for (int index = 0; index < this.refPoints.size() - 1; index++) {
			int i = index;
			// System.out.println("doing pivot " + i);
			final int remainingDataSize = allData.size();
			int chsize = index < missing ? chunkSize + 1 : chunkSize;

			T pivot = this.refPoints.get(i);
			ObjectWithDistance<T>[] owds = new ObjectWithDistance[remainingDataSize];

			for (int notI = 0; notI < remainingDataSize; notI++) {
				final T point = allData.get(notI);
				owds[notI] = new ObjectWithDistance<>(point,
						this.metric.distance(pivot, point));
			}

			Quicksort.placeOrdinal(owds, chsize);

			for (int x = 0; x < chsize; x++) {
				T next = owds[x].getValue();
				DataPoint<T> dp = this.results.get(next);
				dp.refs[iteration] = i;
				dp.dists[iteration] = owds[x].getDistance();

				allData.remove(next);
			}
		}
	}

	private void highDistributionByRefPoint(List<T> witnesses, int iteration) {

		List<T> allData = new ArrayList<>();
		allData.addAll(witnesses);
		int chunkSize = witnesses.size() / this.refPoints.size();

		// System.out.println("chunk size:" + chunkSize);
		int missing = witnesses.size() % this.refPoints.size();

		// TODO Auto-generated method stub
		List<T> unplaced = new ArrayList<>();
		for (int index = 0; index < this.refPoints.size() - 1; index++) {
			int i = index;
			// System.out.println("doing pivot " + i);
			final int remainingDataSize = allData.size();
			int chsize = index < missing ? chunkSize + 1 : chunkSize;

			T pivot = this.refPoints.get(i);
			ObjectWithDistance<T>[] owds = new ObjectWithDistance[remainingDataSize];

			for (int notI = 0; notI < remainingDataSize; notI++) {
				final T point = allData.get(notI);
				owds[notI] = new ObjectWithDistance<>(point,
						this.metric.distance(pivot, point));
			}

			Quicksort.placeOrdinal(owds, owds.length - chsize);

			// loop should take chsize values out of allData
			for (int x = owds.length - chsize; x < owds.length; x++) {
				T next = owds[x].getValue();
				DataPoint<T> dp = this.results.get(next);
				dp.refs[iteration] = i;
				dp.dists[iteration] = owds[x].getDistance();

				allData.remove(next);
			}
		}
	}

	private void lowAltitude(List<T> witnesses, int iteration,
			int apexCoordinate) {
		for (T datum : witnesses) {
			DataPoint<T> dp = this.results.get(datum);

			int existing = (iteration + 1) % 1;
			int p1 = dp.refs[existing];
			double d1 = dp.dists[existing];

			for (int i = 0; i < this.refPoints.size(); i++) {
				double bestAlt = Double.MAX_VALUE;
				if (i != p1) {
					double d2 = this.metric.distance(datum,
							this.refPoints.get(i));
					NdimSimplex<T> sim = this.pivSimplexTable[p1][i];
					double[] dists = { d1, d2 };
					double[] ap = sim.getApex(dists);
					if (ap[apexCoordinate] < bestAlt) {
						bestAlt = ap[apexCoordinate];
						dp.refs[iteration] = i;
						dp.dists[iteration] = d2;
					}
				}
			}
		}
	}

	protected void assign2ndReferenceToData1(List<T> witnesses) {
		int iteration = 1;

		List<T> allData = new ArrayList<>();
		allData.addAll(witnesses);
		int chunkSize = witnesses.size() / this.refPoints.size();
		int missing = witnesses.size() % this.refPoints.size();

		// TODO Auto-generated method stub
		List<T> unplaced = new ArrayList<>();

		for (int index = 0; index < this.refPoints.size() - 1; index++) {
			int i = this.refPoints.size() - (index + 1);
			// System.out.println("doing pivot " + i);
			final int remainingDataSize = allData.size();
			int chsize = index < missing ? chunkSize + 1 : chunkSize;

			T pivot = this.refPoints.get(i);
			ObjectWithDistance<T>[] owds = new ObjectWithDistance[remainingDataSize];

			for (int notI = 0; notI < remainingDataSize; notI++) {
				final T point = allData.get(notI);
				owds[notI] = new ObjectWithDistance<>(point,
						this.metric.distance(pivot, point));
			}

			Quicksort.placeOrdinal(owds, chsize);

			for (int x = 0; x < chsize; x++) {
				T next = owds[x].getValue();
				DataPoint<T> dp = this.results.get(next);
				dp.refs[iteration] = i;
				dp.dists[iteration] = owds[x].getDistance();

				allData.remove(next);
				if (dp.refs[0] == dp.refs[1]) {
					dp.refs[1] = (dp.refs[0] + 1) % this.refPoints.size();
					// unplaced.add(next);
				}
			}
		}
		// for (T duplicate : unplaced) {
		// DataPoint<T> dp = this.results.get(duplicate);
		// // dp.refs[1] = (dp.refs[0] + 1) % this.refPoints.size();
		// // dp.dists[1] = metric.distance(duplicate,
		// // this.refPoints.get(dp.refs[1]));
		// double closest = Double.MAX_VALUE;
		// for (int refPointIndex = 0; refPointIndex < this.refPoints.size();
		// refPointIndex++) {
		// if (refPointIndex != dp.refs[0]) {
		// double dist = this.metric.distance(
		// this.refPoints.get(refPointIndex), duplicate);
		// if (dist < closest) {
		// dp.refs[1] = refPointIndex;
		// dp.dists[1] = dist;
		// closest = dist;
		// }
		// }
		// }
		// }
	}

	private void removeDuplicates(List<T> data) {
		for (T datum : data) {
			DataPoint<T> dp = this.results.get(datum);

			if (dp.refs[0] == dp.refs[1]) {
				dp.refs[1] = (dp.refs[0] + 1) % this.refPoints.size();
			}
		}
	}

}
