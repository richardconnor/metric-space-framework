package extreme_fourpoint;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.List;

import eu.similarity.msc.data.cartesian.CartesianPoint;

public class Oracle {

	public static void main(String[] a) throws ClassNotFoundException,
			IOException {
		ExpContext ec = ExpContext.easyColorsEc(128);
		String nasaOracleFileName = "/Volumes/Data/superExtreme/full_sets/nasa128_3k_3nn.xxx";
		String oracleFileName = "/Volumes/Data/superExtreme/full_sets/colors128_3k_3nn.xxx";
		String badOracleFileName = "/Volumes/Data/superExtreme/full_sets/colors128_3k_t.xxx";
		getData(ec, oracleFileName);
	}

	public static void getData(ExpContext ec, String temp_file_name)
			throws ClassNotFoundException, IOException {
		String surDataTable = temp_file_name;
		FileInputStream fis2 = new FileInputStream(surDataTable);
		ObjectInputStream ois2 = new ObjectInputStream(fis2);
		StoredExclusions se = (StoredExclusions) ois2.readObject();
		fis2.close();

		float[][] pivDists = ec.getPivotDistances();

		int dataSize = se.dist1s.length;

		int count = 0;
		int trueCount = 0;

		for (int i = 0; i < 5000; i++) {
			int ref1 = se.piv1s[i];
			int ref2 = se.piv2s[i];
			float[] apex = { se.dist1s[i], se.dist2s[i] };
			float pDist = pivDists[ref1][ref2];

			System.out.println(ref1 + "\t" + ref2 + "\t" + pDist + "\t"
					+ apex[0] + "\t" + apex[1]);
		}

		// System.out.println();
	}
}
