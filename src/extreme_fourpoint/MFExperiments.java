package extreme_fourpoint;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.ejml.simple.SimpleMatrix;

import searchStructures.ObjectWithDistance;
import searchStructures.Quicksort;
import searchStructures.VPTree;
import sisap_2017_experiments.NdimSimplex;
import coreConcepts.CountedMetric;
import coreConcepts.Metric;
import dataPoints.cartesian.Euclidean;
import dataPoints.cartesian.NamedCartesianPoint;
import dataPoints.cartesian_float.CartesianPointEns;
import dataPoints.cartesian_float.CartesianPointL2Norm;
import dataPoints.cartesian_float.CosineFp;
import dataPoints.cartesian_float.JensenShannon;
import dataSets.fileReaders.NamedCartesianPointFileReader;
import eu.similarity.msc.data.cartesian.CartesianPoint;

public class MFExperiments {

	private static class NNFileWriter {
		static PrintWriter pw = null;

		public NNFileWriter(String dirPath) {

		}

		public void printNNdists(int id,
				ObjectWithDistance<Integer>[] owdsArray, int maxNumber) {

			if (id % 1000 == 0) {
				String nn20FileName = surrogatePath + "30dimNN20/" + id / 1000
						+ ".txt";
				try {
					pw = new PrintWriter(nn20FileName);
				} catch (FileNotFoundException e) {
					throw new RuntimeException(
							"can't open PrintWriter for file " + id / 1000);
				}
			}

			pw.print(id);
			for (int i = 0; i < Math.min(owdsArray.length, maxNumber); i++) {
				pw.print("\t" + owdsArray[i].getValue());
				pw.print("\t" + owdsArray[i].getDistance());
			}
			pw.println();

			if (id % 1000 == 999) {
				pw.close();
			}
		}

	}

	static String mfndIdsPath = "/Volumes/Data/mirflickr/mfnd_id_files/";
	static String dataPath = "/Volumes/Data/alexnet/mfvals/";
	static String expPath = "/Volumes/Data/alexnet/mf_experiments/";
	static String surrogatePath = "/Volumes/Data/alexnet/mf_experiments/apexCollections/";
	static String[] files = { "duplicates", "IND_clusters", "NIND_clusters" };

	@SuppressWarnings("boxing")
	public static List<List<Integer>> getClusters(String clusterFile)
			throws IOException {
		List<List<Integer>> res = new ArrayList<>();
		final String fName = mfndIdsPath + clusterFile + ".txt";
		LineNumberReader lr = new LineNumberReader(new FileReader(fName));
		String nextLine = lr.readLine();
		while (nextLine != null) {
			List<Integer> cluster = new ArrayList<>();
			Scanner sc = new Scanner(nextLine);
			while (sc.hasNextInt()) {
				cluster.add(sc.nextInt());
			}
			sc.close();
			res.add(cluster);
			nextLine = lr.readLine();
		}
		lr.close();
		return res;
	}

	public static Map<Integer, double[]> getDupData()
			throws FileNotFoundException, IOException, ClassNotFoundException {
		String objFileName = expPath + "dupData.obj";
		ObjectInputStream obr = new ObjectInputStream(new FileInputStream(
				objFileName));
		@SuppressWarnings("unchecked")
		final Map<Integer, double[]> res = (Map<Integer, double[]>) obr
				.readObject();
		obr.close();
		return res;
	}

	@SuppressWarnings("boxing")
	public static Set<Integer> getDupIds() throws FileNotFoundException,
			IOException, ClassNotFoundException {
		final String objFileName = expPath + "dupIds.obj";
		File objFile = new File(objFileName);
		if (objFile.exists()) {
			ObjectInputStream obr = new ObjectInputStream(new FileInputStream(
					objFileName));
			@SuppressWarnings({ "unchecked" })
			final Set<Integer> res = (Set<Integer>) obr.readObject();
			obr.close();
			return res;
		} else {
			Set<Integer> ids = new HashSet<>();
			for (String s : files) {
				final String fName = mfndIdsPath + s + ".txt";
				LineNumberReader lr = new LineNumberReader(
						new FileReader(fName));
				String nextLine = lr.readLine();
				while (nextLine != null) {
					Scanner sc = new Scanner(nextLine);
					while (sc.hasNextInt()) {
						ids.add(sc.nextInt());
					}
					sc.close();
					nextLine = lr.readLine();
				}
				lr.close();
			}
			ObjectOutputStream obr = new ObjectOutputStream(
					new FileOutputStream(objFileName));
			obr.writeObject(ids);
			obr.close();
			return ids;
		}
	}

	public static void getNearDistortions(Metric<CartesianPoint> metric, int dim)
			throws FileNotFoundException, ClassNotFoundException, IOException {

		Map<Integer, double[]> rands = getRandData();
		Iterator<double[]> randIt = rands.values().iterator();
		Map<Integer, double[]> dupes = getDupData();
		List<List<Integer>> clusters = getClusters(files[1]);

		List<CartesianPoint> pivots = new ArrayList<>();
		for (int i = 0; i < dim; i++) {
			pivots.add(new CartesianPoint(randIt.next()));
		}
		NdimSimplex<CartesianPoint> simp = new NdimSimplex<>(metric, pivots);

		for (List<Integer> ids : clusters) {
			CartesianPoint p1 = new CartesianPoint(dupes.get(ids.get(0)));
			CartesianPoint p2 = new CartesianPoint(dupes.get(ids.get(1)));

			double[] apex1 = simp.getApex(p1);
			double[] apex2 = simp.getApex(p2);

			double[] bounds = NdimSimplex.getBounds(apex1, apex2);

			System.out.println(bounds[0] + "\t" + metric.distance(p1, p2)
					+ "\t" + bounds[1]);
		}
	}

	public static Map<Integer, double[]> getRandData()
			throws FileNotFoundException, IOException, ClassNotFoundException {
		String objFileName = expPath + "randData.obj";
		ObjectInputStream obr = new ObjectInputStream(new FileInputStream(
				objFileName));
		@SuppressWarnings("unchecked")
		final Map<Integer, double[]> res = (Map<Integer, double[]>) obr
				.readObject();
		obr.close();
		return res;
	}

	public static void getRandDistortions(Metric<CartesianPoint> metric, int dim)
			throws FileNotFoundException, ClassNotFoundException, IOException {

		Map<Integer, double[]> rands = getRandData();
		Iterator<double[]> randIt = rands.values().iterator();

		List<CartesianPoint> pivots = new ArrayList<>();
		for (int i = 0; i < dim; i++) {
			pivots.add(new CartesianPoint(randIt.next()));
		}
		NdimSimplex<CartesianPoint> simp = new NdimSimplex<>(metric, pivots);

		CartesianPoint prev = new CartesianPoint(randIt.next());
		double[] prevApex = simp.getApex(prev);
		while (randIt.hasNext()) {
			CartesianPoint p = new CartesianPoint(randIt.next());
			double[] apex = simp.getApex(p);

			double[] bounds = NdimSimplex.getBounds(apex, prevApex);

			System.out.println(bounds[0] + "\t" + metric.distance(p, prev)
					+ "\t" + bounds[1]);

			prev = p;
			prevApex = apex;
		}
	}

	@SuppressWarnings("boxing")
	public static Set<Integer> getRandIds() throws FileNotFoundException,
			ClassNotFoundException, IOException {
		Set<Integer> dups = getDupIds();
		Random r = new Random();
		final String objFileName = expPath + "randIds.obj";
		File objFile = new File(objFileName);
		if (objFile.exists()) {
			ObjectInputStream obr = new ObjectInputStream(new FileInputStream(
					objFileName));
			@SuppressWarnings("unchecked")
			final Set<Integer> res = (Set<Integer>) obr.readObject();
			obr.close();
			return res;
		} else {
			Set<Integer> ids = new HashSet<>();
			while (ids.size() < 5000) {
				int next = r.nextInt(1000 * 1000);
				if (!dups.contains(next)) {
					ids.add(next);
				}
			}
			ObjectOutputStream obr = new ObjectOutputStream(
					new FileOutputStream(objFileName));
			obr.writeObject(ids);
			obr.close();
			return ids;
		}
	}

	public static void main(String[] a) throws Exception {
		// Set<Integer> dupIds = getDupIds();
		// Set<Integer> randIds = getRandIds();

		// writeDupAndRandDataFiles();
		// System.out.println("got " + randIds.size() + " ids");

		// Map<Integer, double[]> dupData = getDupData();
		// Map<Integer, double[]> randData = getRandData();

		// Metric<CartesianPoint> metric = new Euclidean<>();
		// Metric<float[]> cos = new dataPoints.floatArray.CosineFp();

		// printIndDists(dupData, cos);
		// printRandDists(randData, metric);
		// getNearDistortions(metric, 30);

		// writeApexFiles(100, new CosineNormalised<>());

		// queryApexSet(30);

		// playWithCayleyMenger();

		/*
		 * doing this just now...
		 */
		// queryApexSet(100, 1, 161);
		// queryApexSetFloats(30, "Cos", 10, 29, 330);
		// queryApexSetFloats(30, "Cos", 10, 330, 660);
		// queryApexSetFloats(30, "Cos", 10, 660, 1000);

		final int[] starts = { 129, 141, 146 };
		final int[] ends = { 150, 146, 151 };
		// ExecutorService threadPool = Executors.newFixedThreadPool(3);

		final Map<Integer, float[]> x = getRawValues();
		System.out.println("raw values got");

		// for (final int i = 0; i < 3; i++) {
		// Future<?> fin = threadPool.submit(new Runnable() {
		// @Override
		// public void run() {
		// try {
		createTrueCosFiles(x, 267, 500);
		// } catch (Exception e) {
		// System.out.println("oops thread " + i + " went wrong");
		// }
		// }
		// });
		// }

		// queryApexSetFloats(100, "Cos", 10, starts[ii], ends[ii]);

		// queryApexSetFloats(100, "Cos", 10, 63, 350);
		// queryApexSetFloats(100, "Cos", 10, 411, 700);
		// queryApexSetFloats(100, "Cos", 10, 700, 1000);

		// queryApexSetFloats(100, "", 10, 636, 770);
		// queryApexSetFloats(100, "", 10, 770, 900);
		// queryApexSetFloats(100, "", 10, 900, 1000);

		// queryApexSetFloats(30, "", 10, 8, 330);
		// queryApexSetFloats(30, "", 10, 330, 660);
		// queryApexSetFloats(30, "", 10, 660, 1000);

		// List<NamedCartesianPoint> d = getAllData(30);
		// checkDataSize(d);
		// write20NNfiles();

		// long t0 = System.currentTimeMillis();
		//
		// Map<Integer, float[]> x = getRawValues();
		// System.out.println("raw values got" + ":"
		// + (System.currentTimeMillis() - t0));
		// long t0 = System.currentTimeMillis();

		// createTrueEucFiles(t0, x, 390);

		// createTrueCosFiles(t0, x, 115);
		// createTrueCosFiles(t0, x, 500);
		// createTrueCosFiles(t0, x, 800);

		// createTrueJsdFiles(t0, x, 0);
	}

	private static void checkDataSize(List<NamedCartesianPoint> d) {
		System.out.println(d.size());
		int ptr = 0;
		for (NamedCartesianPoint p : d) {
			if (Integer.parseInt(p.getName()) != ptr++) {
				System.out.println("didn't find " + (ptr - 1));
				break;
			}
		}
	}

	private static void createTrueCosFiles(Map<Integer, float[]> x,
			int startFile, int endFile) throws FileNotFoundException {

		List<CartesianPointL2Norm> l0 = new ArrayList<>();
		for (float[] f : x.values()) {
			l0.add(new CartesianPointL2Norm(f));
		}
		Metric<CartesianPointL2Norm> cos = new CosineFp<>();
		CountedMetric<CartesianPointL2Norm> cMet0 = new CountedMetric<>(cos);

		doNNFiles(cos, l0, cMet0, startFile, endFile, "cosNNTrue");
	}

	private static void createTrueEucFiles(Map<Integer, float[]> x,
			int startFile, int endFile) throws FileNotFoundException {
		List<float[]> l = new ArrayList<>();
		for (float[] f : x.values()) {
			l.add(f);
		}
		System.out.println("list made");
		Metric<float[]> euc = new dataPoints.floatArray.Euclidean();
		CountedMetric<float[]> cMet = new CountedMetric<>(euc);

		doNNFiles(euc, l, cMet, startFile, endFile, "eucNNTrue");
	}

	@Deprecated
	/*
	 * can't use this yet over negative values, need to think hard!
	 */
	private static void createTrueJsdFiles(long t0, Map<Integer, float[]> x,
			int startFile, int endFile) throws FileNotFoundException {

		List<CartesianPointEns> l0 = new ArrayList<>();
		int ptr = 0;
		for (float[] f : x.values()) {
			try {
				l0.add(new CartesianPointEns(f));
			} catch (Throwable e) {
				throw new RuntimeException("CartesianPointEns build failed at "
						+ ptr);
			}
			ptr++;
		}
		Metric<CartesianPointEns> cos = new JensenShannon<>();
		CountedMetric<CartesianPointEns> cMet0 = new CountedMetric<>(cos);

		doNNFiles(cos, l0, cMet0, startFile, endFile, "jsdNNTrue");
	}

	private static <T> void doNNFiles(Metric<T> metric, List<T> l,
			CountedMetric<T> cMet, int startFile, int endFile, String dirName)
			throws FileNotFoundException {
		VPTree<T> vpt = new VPTree<>(l, cMet);
		cMet.reset();
		System.out.println("vpt built");

		for (int fileNo = startFile; fileNo < endFile; fileNo++) {
			System.out.println("opening printwriter " + fileNo);
			PrintWriter pw = new PrintWriter(expPath + dirName + "/" + fileNo
					+ ".txt");
			for (int i = 0; i < 1000; i++) {
				final int queryId = fileNo * 1000 + i;
				// vpt.thresholdQueryByReference(l.get(queryId), 30);
				List<Integer> res = vpt.nearestNeighbour(l.get(queryId), 11);
				if (res.size() > 1) { // redundant but harmless...!
					pw.print(queryId);
					for (int r : res) {
						if (r != queryId) {
							pw.print("\t" + r);
							double dist = metric.distance(l.get(queryId),
									l.get(r));
							pw.print("\t" + (float) dist);
						}
					}
					pw.println();
				}
			}
			System.out.println("closing printwriter " + fileNo);
			pw.close();
		}

	}

	/**
	 * @param i
	 *            the filename
	 * @return the layer23 raw values for file number i, containg values 1 *
	 *         1000 (inc) to (i + 1) * 1000 (exc)
	 * @throws Exception
	 */
	private static NamedCartesianPointFileReader getLayer23Data(int i)
			throws Exception {
		final String fileName = dataPath + i + ".txt";
		NamedCartesianPointFileReader nfr = new NamedCartesianPointFileReader(
				fileName, false);
		System.out.println("got " + nfr.size() + " data from " + fileName);
		return nfr;
	}

	/**
	 * @param file
	 *            number from 0 to 9
	 * 
	 * @return 100k 1000-dimensional data points from MirFlick/AlexNet
	 * @throws FileNotFoundException
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	public static Map<Integer, float[]> getRawFile(int file)
			throws FileNotFoundException, IOException, ClassNotFoundException {
		System.out.println("getting " + file);
		FileInputStream fos = new FileInputStream(
				"/Volumes/Data/alexnet/mvValsJavaMap/rawValsMapF" + file
						+ ".dat");
		ObjectInputStream oos = new ObjectInputStream(fos);
		Map<Integer, float[]> res = (Map<Integer, float[]>) oos.readObject();
		oos.close();
		return res;
	}

	private static Map<Integer, float[]> getRawValues() throws Exception {
		Map<Integer, float[]> res = new TreeMap<>();
		for (int file = 0; file < 10; file++) {
			Map<Integer, float[]> f = getRawFile(file);
			res.putAll(f);
		}
		return res;
	}

	private static List<NamedCartesianPoint> getSurrogateDataSet(int dim)
			throws Exception {
		List<NamedCartesianPoint> dat = new ArrayList<>();
		for (int i = 0; i <= 999; i++) {
			NamedCartesianPointFileReader nfr = new NamedCartesianPointFileReader(
					surrogatePath + dim + "dim/" + i + ".txt", false);
			dat.addAll(nfr);
			System.out.print(".");
			if (i != 0 && i % 100 == 0) {
				System.out.println(i / 100);
			}
		}
		return dat;
	}

	private static List<float[]> getSurrogateDataSetFloat(int dim, String metric)
			throws Exception {
		List<float[]> dat = new ArrayList<>();
		for (int i = 0; i <= 999; i++) {
			NamedCartesianPointFileReader nfr = new NamedCartesianPointFileReader(
					surrogatePath + dim + "dim" + metric + "/" + i + ".txt",
					false);
			for (CartesianPoint p : nfr) {
				double[] ds = p.getPoint();
				float[] fs = new float[ds.length];
				for (int j = 0; j < ds.length; j++) {
					fs[j] = (float) ds[j];
				}
				dat.add(fs);
			}
			System.out.print(".");
			if (i != 0 && i % 100 == 0) {
				System.out.println(i / 100);
			}
		}
		return dat;
	}

	@SuppressWarnings("boxing")
	private static void getTrueValues() throws Exception {
		// FileInputStream fis = new FileInputStream(
		// "/Volumes/Data/alexnet/mvValsJavaMap/rawValsMap.dat");
		// ObjectInputStream ois = new ObjectInputStream(fis);
		// Map<Integer, double[]> res = (Map<Integer, double[]>)
		// ois.readObject();
		// ois.close();

		/*
		 * going to create 10 files each with 100k entries
		 */
		for (int file = 0; file < 10; file++) {
			Map<Integer, float[]> res = new HashMap<>();

			long t0 = System.currentTimeMillis();
			for (int i = 100 * file; i < (100 * file) + 100; i++) {
				NamedCartesianPointFileReader nf0 = getLayer23Data(i);
				for (NamedCartesianPoint p : nf0) {
					int v = Integer.parseInt(p.getName());
					final double[] point = p.getPoint();
					final int length = point.length;
					final float[] pointf = new float[length];
					for (int x = 0; x < length; x++) {
						pointf[x] = (float) point[x];
					}
					res.put(v, pointf);
				}
				System.out.print("file " + i);
				System.out.println(" which took "
						+ (System.currentTimeMillis() - t0));

			}
			FileOutputStream fos = new FileOutputStream(
					"/Volumes/Data/alexnet/mvValsJavaMap/rawValsMapF" + file
							+ ".dat");
			ObjectOutputStream oos = new ObjectOutputStream(fos);
			oos.writeObject(res);
			oos.close();
			System.out.println("res for " + file + " has "
					+ res.keySet().size() + " entries");
		}
	}

	private static void playWithCayleyMenger() throws FileNotFoundException,
			ClassNotFoundException, IOException {
		int dimension = 3;

		Iterator<double[]> rands = getRandData().values().iterator();
		Map<Integer, double[]> close = getDupData();

		double[] p1 = close.get(0);

		// Map<Integer, float[]> data = getRawFile(0);

		System.out.println();

		rands.next();
		List<double[]> pivs = new ArrayList<>();
		for (int i = 0; i < dimension - 1; i++) {
			pivs.add(rands.next());
		}

		Metric<double[]> euc = new dataPoints.doubleArray.Euclidean();
		// Metric<double[]> js = new
		// dataPoints.doubleArray.JensenShannonBaseE();
		Metric<double[]> metric = euc;

		double[][] ar = new double[dimension + 2][dimension + 2];
		for (int i = 0; i < dimension + 2; i++) {
			ar[i][i] = 0;
		}

		for (int i = 1; i < dimension + 2; i++) {
			ar[0][i] = 1;
			ar[i][0] = 1;
		}

		for (int i = 0; i < dimension - 2; i++) {
			for (int j = i + 1; j < dimension - 1; j++) {
				double d = metric.distance(pivs.get(i), pivs.get(j));
				ar[i + 1][j + 1] = d * d;
				ar[j + 1][i + 1] = d * d;
			}
		}

		double threshold = 15;
		ar[dimension][dimension + 1] = threshold * threshold;
		ar[dimension + 1][dimension] = threshold * threshold;

		long t0 = System.currentTimeMillis();
		int matches = 0;
		for (int id1 : close.keySet()) {
			double[] im1 = close.get(id1);
			for (int i = 0; i < dimension - 1; i++) {
				double d1 = metric.distance(pivs.get(i), im1);
				ar[dimension + 1][i + 1] = d1 * d1;
				ar[i + 1][dimension + 1] = d1 * d1;
			}
			// System.out.print(id1);
			for (int id2 : close.keySet()) {
				if (id1 != id2) {
					double[] im2 = close.get(id2);
					for (int i = 0; i < dimension - 1; i++) {
						double d2 = metric.distance(pivs.get(i), im2);
						ar[dimension][i + 1] = d2 * d2;
						ar[i + 1][dimension] = d2 * d2;
					}

					SimpleMatrix sm = new SimpleMatrix(ar);
					double det = sm.determinant();
					if (dimension % 2 == 0 ? det < 0 : det > 0) {
						matches++;
						// System.out.print("\t" + id2);
					}
				}
			}
			// System.out.println();
		}
		System.out.println(System.currentTimeMillis() - t0);
		System.out.println("for " + close.size() * close.size() + " queries");
		System.out.println("and " + matches + " matches");

		/*
		 * time the same for original distances
		 */
		//
		// long t0 = System.currentTimeMillis();
		// int matches = 0;
		// for (int id1 : close.keySet()) {
		// double[] im1 = close.get(id1);
		// // for (int i = 0; i < dimension - 1; i++) {
		// // double d1 = euc.distance(pivs.get(i), im1);
		// // ar[dimension + 1][i + 1] = d1 * d1;
		// // ar[i + 1][dimension + 1] = d1 * d1;
		// // }
		// // System.out.print(id1);
		// for (int id2 : close.keySet()) {
		// if (id1 != id2) {
		// double[] im2 = close.get(id2);
		// // for (int i = 0; i < dimension - 1; i++) {
		// // double d2 = euc.distance(pivs.get(i), im2);
		// // ar[dimension][i + 1] = d2 * d2;
		// // ar[i + 1][dimension] = d2 * d2;
		// // }
		//
		// // SimpleMatrix sm = new SimpleMatrix(ar);
		// // double det = sm.determinant();
		// // if (dimension % 2 == 0 ? det < 0 : det > 0) {
		// // System.out.print("\t" + id2);
		// // }
		// if (euc.distance(im1, im2) < threshold) {
		// matches++;
		// // System.out.print("\t" + id2);
		// }
		// }
		// }
		// // System.out.println();
		// }
		// System.out.println(System.currentTimeMillis() - t0);
		// System.out.println("for " + close.size() * close.size() +
		// " queries");
		// System.out.println("and " + matches + " matches");

		// System.out.println();
		// for (double[] ds : ar) {
		// for (double d : ds) {
		// System.out.print(d + "\t");
		// }
		// System.out.println();
		// }
	}

	private static void printIndDists(Map<Integer, double[]> dupData,
			Metric<CartesianPoint> euc) throws IOException {
		List<List<Integer>> indClusters = getClusters(files[2]);
		for (List<Integer> cl : indClusters) {
			final Integer id1 = cl.get(0);
			double[] d1 = dupData.get(id1);
			final Integer id2 = cl.get(1);
			double[] d2 = dupData.get(id2);
			CartesianPoint p1 = new CartesianPoint(d1);
			CartesianPoint p2 = new CartesianPoint(d2);
			System.out.println(id1 + "\t" + id2 + "\t" + euc.distance(p1, p2));
		}
	}

	private static void printRandDists(Map<Integer, double[]> randData,
			Metric<CartesianPoint> euc) throws FileNotFoundException,
			ClassNotFoundException, IOException {
		Set<Integer> ids = getRandIds();
		Iterator<Integer> itIt = ids.iterator();
		while (itIt.hasNext()) {
			final Integer id1 = itIt.next();
			double[] d1 = randData.get(id1);
			final Integer id2 = itIt.next();
			double[] d2 = randData.get(id2);
			CartesianPoint p1 = new CartesianPoint(d1);
			CartesianPoint p2 = new CartesianPoint(d2);
			System.out.println(id1 + "\t" + id2 + "\t" + euc.distance(p1, p2));
		}
	}

	private static void queryApexSet(int dim, int nn, int startFile)
			throws Exception {
		List<NamedCartesianPoint> dat = getSurrogateDataSet(dim);
		// System.out.println();
		CountedMetric<NamedCartesianPoint> cm = new CountedMetric<>(
				new Euclidean<NamedCartesianPoint>());
		VPTree<NamedCartesianPoint> vpt = new VPTree<>(dat, cm);
		// HPT_random<NamedCartesianPoint> hpt = new HPT_random<>(cm, true,
		// Arity.log, true);
		// NarySearchTree<NamedCartesianPoint, ?, ?> nst = new NarySearchTree<>(
		// dat, hpt);

		// SearchIndex<NamedCartesianPoint> si = nst;

		cm.reset();
		// int queries = 0;
		long t0 = System.currentTimeMillis();
		// for (NamedCartesianPoint q : dat) {
		PrintWriter pw = null;
		for (int i = startFile * 1000; i < 1000 * 1000; i++) {
			if (i % 1000 == 0) {
				if (pw != null) {
					pw.close();
				}
				pw = new PrintWriter(surrogatePath + dim + "dim" + nn + "NN/"
						+ i / 1000 + ".txt");
			}
			NamedCartesianPoint q = dat.get(i);
			List<Integer> res = vpt.nearestNeighbour(q, nn + 1);

			// List<NamedCartesianPoint> res = vpt.thresholdSearch(q, 30);
			// queries++;
			if (res.size() > 1) {
				pw.print(q.getName());
				for (int r : res) {
					if (!q.getName().equals(dat.get(r).getName())) {
						pw.print("\t" + dat.get(r).getName());
						pw.print("\t" + (float) cm.distance(q, dat.get(r)));
					}
				}
				pw.println();
			}
		}
		pw.close();
	}

	private static void queryApexSetFloats(int dim, String metric, int nn,
			int startFile, int endFile) throws Exception {
		List<float[]> dat = getSurrogateDataSetFloat(dim, metric);
		// System.out.println();
		CountedMetric<float[]> cm = new CountedMetric<>(
				new dataPoints.floatArray.Euclidean());
		VPTree<float[]> vpt = new VPTree<>(dat, cm);
		// HPT_random<NamedCartesianPoint> hpt = new HPT_random<>(cm, true,
		// Arity.log, true);
		// NarySearchTree<NamedCartesianPoint, ?, ?> nst = new NarySearchTree<>(
		// dat, hpt);

		// SearchIndex<NamedCartesianPoint> si = nst;

		cm.reset();
		// int queries = 0;
		long t0 = System.currentTimeMillis();
		// for (NamedCartesianPoint q : dat) {
		PrintWriter pw = null;
		for (int i = startFile * 1000; i < endFile * 1000; i++) {
			if (i % 1000 == 0) {
				if (pw != null) {
					pw.close();
				}
				pw = new PrintWriter(surrogatePath + dim + "dim" + metric + nn
						+ "NN/" + i / 1000 + ".txt");
			}
			float[] q = dat.get(i);
			List<Integer> res = vpt.nearestNeighbour(q, nn + 1);

			// List<NamedCartesianPoint> res = vpt.thresholdSearch(q, 30);
			// queries++;
			if (res.size() > 1) {
				pw.print(i);
				for (int r : res) {
					if (!(i == r)) {
						pw.print("\t" + r);
						pw.print("\t" + (float) cm.distance(q, dat.get(r)));
					}
				}
				pw.println();
			}
		}
		pw.close();
	}

	/**
	 * opens the threshold search files and writes new 20NN files problem: not
	 * all ids have this many nns
	 *
	 * TODO problem 2: threshold search files don't have the correct id
	 * boundaries
	 * 
	 * @throws IOException
	 */
	@SuppressWarnings("boxing")
	private static void write20NNfiles() throws IOException {
		ObjectWithDistance<Integer>[] empty = new ObjectWithDistance[0];
		NNFileWriter nnfw = new NNFileWriter(surrogatePath + "30dimNN20/");

		Map<Integer, Integer> noMatches = new TreeMap<>();
		int firstFile = 0;
		int nextToWrite = 0;
		for (int fileNo = firstFile; fileNo < 1000; fileNo++) {
			String resFileName = surrogatePath + "30dimClose/" + fileNo
					+ ".txt";
			LineNumberReader lr = new LineNumberReader(new FileReader(
					resFileName));
			String nextLine = null;
			while ((nextLine = lr.readLine()) != null) {
				Scanner s = new Scanner(nextLine);
				int id = s.nextInt();
				if (id != nextToWrite) {
					while (id != nextToWrite) {
						nnfw.printNNdists(nextToWrite, empty, 20);
						noMatches.put(nextToWrite++, 0);
						if (nextToWrite > 1000 * 1000) {
							throw new RuntimeException("too many writes!");
						}
					}
				}
				int matches = 0;
				ArrayList<ObjectWithDistance<Integer>> owds = new ArrayList<>();
				while (s.hasNextInt()) {
					try {
						int match1 = s.nextInt();
						double dist = s.nextDouble();
						ObjectWithDistance<Integer> owd = new ObjectWithDistance<>(
								match1, dist);
						owds.add(owd);
						matches++;
					} catch (Exception e) {
						System.out.println(id + ":" + matches + ":" + e);
					}
				}
				ObjectWithDistance<Integer>[] owdsArray = new ObjectWithDistance[matches];
				owds.toArray(owdsArray);
				Quicksort.sort(owdsArray);

				nnfw.printNNdists(id, owdsArray, 20);

				noMatches.put(nextToWrite++, matches);
				s.close();
			}
			lr.close();
			if (fileNo == 999) {
				while (nextToWrite < 1000 * 1000) {
					nnfw.printNNdists(nextToWrite, empty, 20);
					noMatches.put(nextToWrite++, 0);
				}
			}
		}

		System.out.println(noMatches.keySet().size());
	}

	/**
	 * writes Cartesian coordinates of simplex apexes for all data, based on the
	 * random data point
	 * 
	 * @param dim
	 * @param metric
	 * @throws Exception
	 */
	private static void writeApexFiles(int dim, Metric<CartesianPoint> metric)
			throws Exception {
		Map<Integer, double[]> rands = getRandData();
		Iterator<double[]> randIt = rands.values().iterator();
		List<CartesianPoint> pivots = new ArrayList<>();
		String surPathName = surrogatePath + dim + "dim"
				+ metric.getMetricName() + "/";
		PrintWriter pw = new PrintWriter(surPathName + "simpPoints.txt");

		for (int i = 0; i < dim; i++) {
			final double[] next = randIt.next();
			for (double d : next) {
				pw.print(d + "\t");
			}
			pw.println();
			pivots.add(new CartesianPoint(next));
		}
		pw.close();

		NdimSimplex<CartesianPoint> simp = new NdimSimplex<>(metric, pivots);

		for (int i = 0; i <= 999; i++) {
			final String inputFileName = dataPath + i + ".txt";
			final String outputFileName = surPathName + i + ".txt";
			NamedCartesianPointFileReader nfr = new NamedCartesianPointFileReader(
					inputFileName, false);
			System.out.println("got " + nfr.size() + " data from "
					+ inputFileName);
			PrintWriter pw2 = new PrintWriter(outputFileName);
			for (NamedCartesianPoint ncp : nfr) {
				double[] ap = simp.getApex(ncp);
				pw2.print(ncp.getName());
				for (double d : ap) {
					pw2.print("\t" + (float) d);
				}
				pw2.println();
			}
			pw2.close();
		}
	}

	@SuppressWarnings({ "boxing", "unused" })
	private static void writeDupAndRandDataFiles() throws Exception {
		Set<Integer> dupIds = getDupIds();
		Set<Integer> randIds = getRandIds();

		Map<Integer, double[]> dupData = getDupData();
		Map<Integer, double[]> randData = getRandData();

		for (int i = 0; i <= 999; i++) {
			NamedCartesianPointFileReader nfr = getLayer23Data(i);
			for (NamedCartesianPoint ncp : nfr) {
				int val = Integer.parseInt(ncp.getName());
				if (dupIds.contains(val)) {
					dupData.put(val, ncp.getPoint());
				}
				if (randIds.contains(val)) {
					randData.put(val, ncp.getPoint());
				}
			}
		}

		ObjectOutputStream obr1 = new ObjectOutputStream(new FileOutputStream(
				expPath + "dupData.obj"));
		obr1.writeObject(dupData);
		obr1.close();

		ObjectOutputStream obr2 = new ObjectOutputStream(new FileOutputStream(
				expPath + "randData.obj"));
		obr2.writeObject(randData);
		obr2.close();

	}
}
