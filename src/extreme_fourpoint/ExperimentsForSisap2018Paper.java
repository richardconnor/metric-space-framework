package extreme_fourpoint;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

import searchStructures.SearchIndex;
import searchStructures.VPTree;
import sisap_2017_experiments.NdimSimplex;
import util.Range;
import util.Timer.Command;
import util.Util_ISpaper;
import coreConcepts.Metric;
import dataPoints.cartesian.Euclidean;
import eu.similarity.msc.data.cartesian.CartesianPoint;
import extreme_fourpoint.ExpContext.Data;
import extreme_fourpoint.RefPointsSelector.DataPoint;

public class ExperimentsForSisap2018Paper {

	public static void main(String[] a) throws Exception {

		final ExpContext colors3NN = ExpContext.easyColorsEc(128);
		final ExpContext nasa3NN = ExpContext.easyNasaEc(128);

		ExpContext ec = colors3NN;

		sisapNasaCreateAndQuery();

		// vptQuery(colors3NN);
		// colors3NN.generatePivotTableData();
		// colors3NN.generatePivotTableData();
		// generateSurrogateData(ec, false);
		// for (int i = 0; i < 20; i++) {
		// doSurrogateQueries(ec, 1, true);
		// }
		// doRocQuery(colors3NN);
	}

	//
	// private static ExpContext getYfccContext() {
	// final ExpContext yfcc = new ExpContext(ExpContext.Data.yfcc, 128);
	// yfcc.queryThreshold = 0.75;
	// yfcc.noOfResultsAtThreshold = 117;
	// yfcc.createThreshold = 0;
	// yfcc.filterThreshold = yfcc.queryThreshold;
	// yfcc.kNN = true;
	// yfcc.k = 3;
	// yfcc.t = 40;
	// yfcc.fft = false;
	// yfcc.metric = new Euclidean<>();
	// return yfcc;
	// }

	// private static ExpContext getAlexNetContext(int noOfRefPoints) {
	// final ExpContext alex3nn = new ExpContext(ExpContext.Data.alexnet,
	// noOfRefPoints);
	// alex3nn.queryThreshold = 40;
	// alex3nn.createThreshold = 30;
	// alex3nn.filterThreshold = 25;
	// alex3nn.noOfResultsAtThreshold = 161;
	// alex3nn.kNN = true;
	// alex3nn.k = 3;
	// alex3nn.t = 40;
	// alex3nn.fft = false;
	// alex3nn.metric = new Euclidean<>();
	// return alex3nn;
	// }

	private static void createAndQuery(final ExpContext ec) throws Exception {

		final boolean serialPivots = false;

		printTime("creating colors threshold t1", new Command() {
			@SuppressWarnings("synthetic-access")
			@Override
			public void execute() {
				try {
					// generateSurrogateData5p(colorsT1);
					generateSurrogateData(ec, serialPivots);
					// ec.generatePivotTableData();
				} catch (Exception e) {
					System.out.println("oh dear command failed");
					e.printStackTrace();
				}
			}
		});

		doRocQuery(ec);

	}

	private static void sisapNasaCreateAndQuery() throws Exception {

		int[] refPoints = { 10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 110, 120,
				130, 140, 150 };
		double[] cThresh = ExpContext.nasaThresholds;

		for (int i : refPoints) {
			System.out.println(i + " ref points");
			final ExpContext ec = new ExpContext(Data.nasa, i);
			final boolean serialPivots = false;

			printTime("creating nasa threshold t1", new Command() {
				@SuppressWarnings("synthetic-access")
				@Override
				public void execute() {
					try {
						generateSurrogateData(ec, serialPivots);
						ec.generatePivotTableData();
					} catch (Exception e) {
						System.out.println("oh dear command failed");
						e.printStackTrace();
					}
				}
			});

			for (double d : cThresh) {
				ec.setThresholds(d);
				doSurrogateQueries(ec, 1, true);
			}
		}
	}

	private static void doRocQuery(final ExpContext ec) throws Exception {
		System.out.print("threshold");
		System.out.print("\t");
		System.out.print("1 - specificity");
		System.out.print("\t");
		System.out.println("sensitivity");
		System.out.println("0\t0\t0");

		// total true values: colors t0 = 1858, alexnet/40 is 161
		for (int thresh = 1; thresh <= 20; thresh++) {
			ec.filterThreshold = ec.queryThreshold * ((float) thresh / 20);
			doSurrogateQueries(ec, ec.noOfResultsAtThreshold, false);
		}
	}

	private static void testData(ExpContext ec, String dataName, int refsSize,
			int witnessSize, double threshold, boolean time) throws Exception {
		List<CartesianPoint> pivs = ec.getPivots();
		List<CartesianPoint> wits = ec.getWitnesses();
		List<CartesianPoint> dat = ec.getTestData();

		Metric<CartesianPoint> euc = new Euclidean<>();

		FourPointPivotsChooser<CartesianPoint> pc = new FourPointPivotsChooser<>(
				pivs, euc, threshold, false, 3);
		pc.setWitnesses(wits);

		long t0 = System.currentTimeMillis();
		for (CartesianPoint d : dat) {
			DataPoint<CartesianPoint> x = pc.getDataPoint(d);
			if (!time) {
				System.out.println(x.refs[0] + "\t" + x.refs[1] + "\t"
						+ x.dists[0] + "\t" + x.dists[1] + "\t" + x.radius);
			}
		}
		long t1 = System.currentTimeMillis();
		if (time) {
			System.out.println("that took " + (t1 - t0) + " for " + dat.size()
					+ " values");
		}

	}

	private static void generateSurrogateData(ExpContext ec, boolean serial)
			throws Exception {

		List<CartesianPoint> allPivots = ec.getPivots();
		int refsSize = allPivots.size();

		List<CartesianPoint> wits = ec.getWitnesses();
		List<CartesianPoint> dat = ec.getBigData();

		List<CartesianPoint> pivs = !ec.fft ? allPivots.subList(0, refsSize)
				: Util_ISpaper.getFFT(allPivots, ec.metric, refsSize);

		Metric<CartesianPoint> euc = new Euclidean<>();

		RefPointsSelector<CartesianPoint> pivotsChooser = serial ? (new FourPointPivotsChooserSerial<>(
				pivs, euc, ec.createThreshold, !ec.kNN, ec.k))
				: (new FourPointPivotsChooser<>(pivs, euc, ec.createThreshold,
						!ec.kNN, ec.k));

		pivotsChooser.setWitnesses(wits);

		StoredExclusions se = new StoredExclusions();

		int size = dat.size();
		se.dist1s = new float[size];
		se.dist2s = new float[size];
		se.piv1s = new short[size];
		se.piv2s = new short[size];
		for (int i = 0; i < dat.size(); i++) {
			if (i % 100 == 0) {
				// System.out.println("doing " + i);
			}
			CartesianPoint d = dat.get(i);
			DataPoint<CartesianPoint> x = pivotsChooser.getDataPoint(d);
			se.dist1s[i] = (float) x.apex[0];
			se.dist2s[i] = (float) x.apex[1];
			se.piv1s[i] = (short) x.refs[0];
			se.piv2s[i] = (short) x.refs[1];
		}
		OutputStream fw = new FileOutputStream(ec.getSurDataFileName(serial));
		ObjectOutputStream oos = new ObjectOutputStream(fw);
		oos.writeObject(se);
		oos.close();
	}

	private static void generateSurrogateData5p(ExpContext ec) throws Exception {

		List<CartesianPoint> allPivots = ec.getPivots();
		int refsSize = allPivots.size();
		List<CartesianPoint> wits = ec.getWitnesses();
		List<CartesianPoint> dat = ec.getBigData();

		List<CartesianPoint> pivs = !ec.fft ? allPivots.subList(0, refsSize)
				: Util_ISpaper.getFFT(allPivots, ec.metric, refsSize);

		Metric<CartesianPoint> euc = new Euclidean<>();

		FivePointPivotsChooser<CartesianPoint> pivotsChooser = new FivePointPivotsChooser<>(
				pivs, euc, ec.createThreshold, !ec.kNN, ec.k);
		pivotsChooser.setWitnesses(wits);

		StoredExclusionsNpoint se = new StoredExclusionsNpoint();

		int size = dat.size();
		se.dists = new float[size][3];
		se.pivs = new short[size][3];
		for (int i = 0; i < dat.size(); i++) {
			if (i % 100 == 0) {
				System.out.println("doing " + i);
			}
			CartesianPoint d = dat.get(i);
			DataPoint<CartesianPoint> x = pivotsChooser.getDataPoint(d);
			se.dists[i][0] = (float) x.apex[0];
			se.dists[i][1] = (float) x.apex[1];
			se.dists[i][2] = (float) x.apex[2];
			se.pivs[i][0] = (short) x.refs[0];
			se.pivs[i][1] = (short) x.refs[1];
			se.pivs[i][2] = (short) x.refs[2];
		}
		OutputStream fw = new FileOutputStream(ec.getSurDataFileName(false)
				+ "5");
		ObjectOutputStream oos = new ObjectOutputStream(fw);
		oos.writeObject(se);
		oos.close();
	}

	public static void doSurrogateQueries5p(ExpContext ec, int totalTrue)
			throws Exception {

		// String pivotDistTable = rootDir + ec.dataset + "/pivotDists" +
		// refsSize
		// + "_" + "_euc" + ".dat";

		String surDataTable = ec.getSurDataFileName(false) + "5";

		// FileInputStream fis1 = new FileInputStream(pivotDistTable);
		FileInputStream fis2 = new FileInputStream(surDataTable);
		// ObjectInputStream ois1 = new ObjectInputStream(fis1);
		ObjectInputStream ois2 = new ObjectInputStream(fis2);

		// float[][] pivDists = (float[][]) ois1.readObject();
		StoredExclusionsNpoint se = (StoredExclusionsNpoint) ois2.readObject();
		// fis1.close();
		fis2.close();
		int dataSize = se.dists.length;

		List<CartesianPoint> queries = ec.getQueries();
		List<CartesianPoint> pivs = ec.getPivots();
		List<CartesianPoint> bigdata = ec.getBigData();
		int refsSize = pivs.size();

		long t0 = System.currentTimeMillis();

		int count = 0;
		int trueCount = 0;

		for (CartesianPoint p : queries) {
			float[] dists = new float[refsSize];
			for (int i = 0; i < refsSize; i++) {
				dists[i] = (float) ec.metric.distance(p, pivs.get(i));
			}

			for (int i = 0; i < dataSize; i++) {
				double[] apex1 = { se.dists[i][0], se.dists[i][1],
						se.dists[i][2] };

				List<CartesianPoint> initialPivots = new ArrayList<>();
				initialPivots.add(pivs.get(se.pivs[i][0]));
				initialPivots.add(pivs.get(se.pivs[i][1]));
				initialPivots.add(pivs.get(se.pivs[i][2]));
				NdimSimplex<CartesianPoint> simp = new NdimSimplex<>(ec.metric,
						initialPivots);
				double[] apex2 = simp.getApex(p);

				double lwb = l2(apex1, apex2);
				if (lwb < ec.filterThreshold) {
					final double trueD = ec.metric.distance(p, bigdata.get(i));
					// System.out.print("\t" + i + "\t" + lwb + "\t" +
					// trueD);
					count++;
					if (trueD <= ec.queryThreshold) {
						// System.out.println(lwb + "\t" + trueD);
						trueCount++;
					}
				}
			}

		}

		System.out.print(ec.filterThreshold);
		System.out.print("\t");
		System.out.print(1 - (float) (count - trueCount)
				/ (queries.size() * dataSize - totalTrue));
		System.out.print("\t");
		System.out.println((float) trueCount / totalTrue);
		// System.out.println((float) (trueCount * 100)
		// / (queries.size() * dataSize) + " percentage trues");
	}

	public static void doSurrogateQueries(ExpContext ec, int totalTrue,
			boolean checkDists) throws Exception {

		String surDataTable = ec.getSurDataFileName(false);
		FileInputStream fis2 = new FileInputStream(surDataTable);
		ObjectInputStream ois2 = new ObjectInputStream(fis2);

		float[][] pivDists = ec.getPivotDistances();
		StoredExclusions se = (StoredExclusions) ois2.readObject();
		fis2.close();
		int dataSize = se.dist1s.length;

		List<CartesianPoint> queries = ec.getQueries();
		List<CartesianPoint> pivs = ec.getPivots();
		List<CartesianPoint> bigdata = ec.getBigData();

		int count = 0;
		int trueCount = 0;

		long t0 = System.currentTimeMillis();
		for (CartesianPoint p : queries) {
			float[] dists = new float[pivs.size()];
			for (int i = 0; i < pivs.size(); i++) {
				dists[i] = (float) ec.metric.distance(p, pivs.get(i));
			}

			for (int i = 0; i < dataSize; i++) {
				float[] apex1 = { se.dist1s[i], se.dist2s[i] };
				float pDist = pivDists[se.piv1s[i]][se.piv2s[i]];
				float d1 = dists[se.piv1s[i]];
				float d2 = dists[se.piv2s[i]];

				float x_offset = (d1 * d1 + pDist * pDist - d2 * d2)
						/ (2 * pDist);
				float y_offset = (float) Math.sqrt(d1 * d1 - x_offset
						* x_offset);
				float[] apex2 = { x_offset, y_offset };
				double lwb = l2_2dim(apex1, apex2);
				if (checkDists && lwb < ec.filterThreshold) {
					final double trueD = ec.metric.distance(p, bigdata.get(i));
					// System.out.print("\t" + i + "\t" + lwb + "\t" +
					// trueD);
					count++;
					if (trueD <= ec.queryThreshold) {
						// System.out.println(lwb + "\t" + trueD);
						trueCount++;
					}
				}
			}

			// System.out.println();
		}
		long t1 = System.currentTimeMillis();

		System.out.print(ec.filterThreshold);
		System.out.print("\t");
		System.out.print((float) (count - trueCount)
				/ (queries.size() * dataSize - totalTrue));
		System.out.print("\t");
		System.out.print((float) trueCount / totalTrue);
		System.out.print("\t");
		System.out.print(t1 - t0);
		System.out.print("\t");
		System.out.print(count / queries.size());
		System.out.println();
	}

	public static double l2_2dim(float[] a, float[] b) {
		float a0 = a[0] - b[0];
		float a1 = a[1] - b[1];

		return Math.sqrt(a0 * a0 + a1 * a1);
	}

	public static double l2(double[] a, double[] b) {
		double acc = 0;
		for (int i = 0; i < a.length; i++) {
			double a0 = a[i] - b[i];
			acc += a0 * a0;
		}
		return Math.sqrt(acc);
	}

	public static void randomHistogram(ExpContext ec) throws Exception {
		Random rand = new Random(0);
		List<CartesianPoint> queries = ec.getQueries();
		List<CartesianPoint> data = ec.getBigData();

		for (int i : Range.range(0, 138912)) {
			CartesianPoint p1 = queries.get(rand.nextInt(queries.size()));
			CartesianPoint p2 = data.get(rand.nextInt(data.size()));
			System.out.println(ec.metric.distance(p1, p2));
		}
	}

	public static void vptQuery(ExpContext ec) throws Exception {
		List<CartesianPoint> queries = ec.getQueries();
		List<CartesianPoint> data = ec.getBigData();
		SearchIndex<CartesianPoint> vpt = new VPTree<>(data, ec.metric);
		long t0 = System.currentTimeMillis();
		int results = 0;
		for (CartesianPoint q : queries) {
			List<CartesianPoint> res = vpt
					.thresholdSearch(q, ec.queryThreshold);
			results += res.size();
		}
		long t1 = System.currentTimeMillis();
		System.out.println((t1 - t0) + " msec per " + queries.size()
				+ " queries");
		System.out.println(results + " results");
	}

	private static void printTime(String what, Command action) {
		long t0 = System.currentTimeMillis();
		action.execute();
		long t1 = System.currentTimeMillis();

		System.out.println(what + " took " + (t1 - t0) + " msecs");
	}

	private static void readYfccFile() throws IOException {
		String filename = "/docsNotOnIcloud/Research/yfcc/data1_20k.txt";
		Reader is = new FileReader(filename);
		LineNumberReader r = new LineNumberReader(is);
		String first = r.readLine();
		System.out.println("chucking " + first);
		float[][] data = new float[20 * 1000][4096];

		for (int i = 0; i < 20 * 1000; i++) {
			if (i % 1000 == 0) {
				System.out.println("doing " + i);
			}
			String line = r.readLine();
			Scanner s = new Scanner(line);
			s.next();
			s.next();

			double acc = 0;
			for (int x = 0; x < 4096; x++) {
				float f = s.nextFloat();
				if (f >= 0) {
					data[i][x] = f;
					acc += f * f;
				}
			}
			float mag = (float) Math.sqrt(acc);

			for (int x = 0; x < 4096; x++) {
				data[i][x] = data[i][x] / mag;
			}
		}
		r.close();

		System.out.println(data[9999][4095]);
		FileOutputStream fos = new FileOutputStream(
				"/docsNotOnIcloud/Research/yfcc/data1_20k.obj");
		ObjectOutputStream oos = new ObjectOutputStream(fos);
		oos.writeObject(data);
		oos.close();
	}

	public static float[] getApex(float pDist, float d1, float d2) {
		float x_offset = (d1 * d1 + pDist * pDist - d2 * d2) / (2 * pDist);
		float y_offset = (float) Math.sqrt(d1 * d1 - x_offset * x_offset);
		float[] apex = { x_offset, y_offset };
		return apex;
	}
}
