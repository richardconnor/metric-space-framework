package extreme_fourpoint;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import searchStructures.ObjectWithDistance;
import searchStructures.Quicksort;
import sisap_2017_experiments.NdimSimplex;
import testloads.TestContext;
import coreConcepts.Metric;
import dataPoints.cartesian.Euclidean;
import eu.similarity.msc.data.cartesian.CartesianPoint;

public class DistributionPivotsChooser<T> extends RefPointsSelector<T> {

	private Map<T, DataPoint<T>> results;
	private NdimSimplex<T>[][] pivSimplexTable;
	private static String temp_file_name = "/Volumes/Data/superExtreme/testData/XXX.xxx";

	DistributionPivotsChooser(List<T> refPoints, Metric<T> metric) {
		super(refPoints, metric);
		this.results = new HashMap<>();
		this.pivSimplexTable = new NdimSimplex[refPoints.size()][refPoints
				.size()];
		for (int i = 0; i < refPoints.size() - 1; i++) {
			for (int j = i + 1; j < refPoints.size(); j++) {
				List<T> refPair = new ArrayList<>();
				refPair.add(refPoints.get(i));
				refPair.add(refPoints.get(j));
				this.pivSimplexTable[i][j] = new NdimSimplex<>(metric, refPair);
			}
		}
	}

	@Override
	public void setWitnesses(List<T> witnesses) {
		// here we need the whole data set in one go
		Random r = new Random();
		System.out.println("this many ref points: " + this.refPoints.size());
		for (T dat : witnesses) {
			DataPoint<T> dp = new DataPoint<>();
			dp.datum = dat;
			dp.refs = new int[2];
			dp.refs[0] = r.nextInt(this.refPoints.size());
			while (dp.refs[1] == dp.refs[0]) {
				dp.refs[1] = r.nextInt(this.refPoints.size());
			}
			// dp.refs[0] = 6;
			// dp.refs[1] = 2;

			dp.dists = new double[2];
			dp.dists[0] = metric.distance(dat, this.refPoints.get(dp.refs[0]));
			dp.dists[1] = metric.distance(dat, this.refPoints.get(dp.refs[1]));
			this.results.put(dat, dp);
		}

		List<T> allData = new ArrayList<>();
		allData.addAll(witnesses);
		System.out.println(allData.size());

		int chunkSize = witnesses.size() / this.refPoints.size();
		System.out.println("chunk size:" + chunkSize);
		int missing = witnesses.size() % this.refPoints.size();
		System.out.println("ref points size: " + refPoints.size());

		assignReferencesToData(allData, chunkSize, missing, true);

		System.out.println("checking: data size " + allData.size());

		allData = new ArrayList<>();
		allData.addAll(witnesses);
		System.out.println(allData.size());

		assignReferencesToData(allData, chunkSize, missing, false);

		System.out.println("checking: data size " + allData.size());
	}

	private void assignReferencesToData(List<T> allData, int chunkSize,
			int missing, boolean first) {
		List<T> unplaced = new ArrayList<>();
		int iteration = first ? 0 : 1;
		for (int index = 0; index < this.refPoints.size() - 1; index++) {
			int i = first ? index : this.refPoints.size() - (index + 1);
			// System.out.println("doing pivot " + i);
			final int remainingDataSize = allData.size();
			int lowerChunkSize = chunkSize % 2 == 1 ? chunkSize / 2 + 1
					: chunkSize / 2;
			if (index < missing) {
				lowerChunkSize++;
			}

			T pivot = this.refPoints.get(i);
			ObjectWithDistance<T>[] owds = new ObjectWithDistance[remainingDataSize];

			for (int notI = 0; notI < remainingDataSize; notI++) {
				final T point = allData.get(notI);
				owds[notI] = new ObjectWithDistance<>(point,
						this.metric.distance(pivot, point));
			}

			Quicksort.placeOrdinal(owds, lowerChunkSize);
			Quicksort.placeOrdinal(owds, owds.length - chunkSize / 2);

			for (int x = 0; x < lowerChunkSize; x++) {
				T next = owds[x].getValue();
				DataPoint<T> dp = this.results.get(next);
				dp.refs[iteration] = i;
				dp.dists[iteration] = owds[x].getDistance();

				allData.remove(next);
				if (!first && dp.refs[0] == dp.refs[1]) {
					unplaced.add(next);
				}

			}
			for (int x = owds.length - chunkSize / 2; x < owds.length; x++) {
				T next = owds[x].getValue();
				DataPoint<T> dp = this.results.get(next);
				dp.refs[iteration] = i;
				dp.dists[iteration] = owds[x].getDistance();

				allData.remove(next);
				if (!first && dp.refs[0] == dp.refs[1]) {
					unplaced.add(next);
				}
			}
			// System.out.println("data size " + allData.size());
		}
		final int lastRefIndex = first ? refPoints.size() - 1 : 0;
		T lastRef = refPoints.get(lastRefIndex);
		System.out.println("doing pivot " + lastRefIndex);
		for (T residual : allData) {
			DataPoint<T> dp = this.results.get(residual);
			dp.refs[iteration] = lastRefIndex;
			dp.dists[iteration] = this.metric.distance(residual,
					this.refPoints.get(lastRefIndex));
			if (!first && dp.refs[0] == dp.refs[1]) {
				unplaced.add(residual);
			}
		}
		for (T duplicate : unplaced) {
			DataPoint<T> dp = this.results.get(duplicate);
			// dp.refs[1] = (dp.refs[0] + 1) % this.refPoints.size();
			// dp.dists[1] = metric.distance(duplicate,
			// this.refPoints.get(dp.refs[1]));
			double closest = Double.MAX_VALUE;
			for (int refPointIndex = 0; refPointIndex < this.refPoints.size(); refPointIndex++) {
				if (refPointIndex != dp.refs[0]) {
					double dist = this.metric.distance(
							this.refPoints.get(refPointIndex), duplicate);
					if (dist < closest) {
						dp.refs[1] = refPointIndex;
						dp.dists[1] = dist;
						closest = dist;
					}
				}
			}
		}
	}

	@Override
	public DataPoint<T> getDataPoint(T datum) {
		// fill in apex values here
		final DataPoint<T> res = this.results.get(datum);
		if (res.refs[0] > res.refs[1]) {
			int temp = res.refs[0];
			res.refs[0] = res.refs[1];
			res.refs[1] = temp;
			double temp1 = res.dists[0];
			res.dists[0] = res.dists[1];
			res.dists[1] = temp1;
		}
		NdimSimplex<T> sim = this.pivSimplexTable[res.refs[0]][res.refs[1]];
		res.apex = sim.getApex(res.dists);
		return res;
	}

	public static void doSurrogateQueries(ExpContext ec, int totalTrue)
			throws Exception {

		String surDataTable = temp_file_name;
		FileInputStream fis2 = new FileInputStream(surDataTable);
		ObjectInputStream ois2 = new ObjectInputStream(fis2);
		StoredExclusions se = (StoredExclusions) ois2.readObject();
		fis2.close();

		float[][] pivDists = ec.getPivotDistances();

		int dataSize = se.dist1s.length;

		List<CartesianPoint> queries = ec.getQueries();
		List<CartesianPoint> pivs = ec.getPivots();
		List<CartesianPoint> bigdata = ec.getBigData();
		if (dataSize != bigdata.size()) {
			throw new Exception("oops...");
		}

		int count = 0;
		int trueCount = 0;

		for (CartesianPoint p : queries) {
			float[] dists = new float[pivs.size()];
			for (int i = 0; i < pivs.size(); i++) {
				dists[i] = (float) ec.metric.distance(p, pivs.get(i));
			}

			for (int i = 0; i < dataSize; i++) {
				float[] apex1 = { se.dist1s[i], se.dist2s[i] };
				float pDist = pivDists[se.piv1s[i]][se.piv2s[i]];
				float d1 = dists[se.piv1s[i]];
				float d2 = dists[se.piv2s[i]];

				float x_offset = (d1 * d1 + pDist * pDist - d2 * d2)
						/ (2 * pDist);
				float y_offset = (float) Math.sqrt(d1 * d1 - x_offset
						* x_offset);
				float[] apex2 = { x_offset, y_offset };
				double lwb = l2_2dim(apex1, apex2);

				if (lwb <= ec.filterThreshold) {
					final double trueD = ec.metric.distance(p, bigdata.get(i));
					// System.out.print("\t" + i + "\t" + lwb + "\t" +
					// trueD);
					count++;
					if (trueD <= ec.queryThreshold) {
						// System.out.println(lwb + "\t" + trueD);
						trueCount++;
					}
				}
			}

			// System.out.println();
		}

		System.out.print(ec.filterThreshold);
		System.out.print("\t");
		System.out.print((float) (count - trueCount)
				/ (queries.size() * dataSize - totalTrue));
		System.out.print("\t");
		System.out.println((float) trueCount / totalTrue);
	}

	public static double l2_2dim(float[] a, float[] b) {
		float a0 = a[0] - b[0];
		float a1 = a[1] - b[1];

		return Math.sqrt(a0 * a0 + a1 * a1);
	}

	private static void doRocQuery(final ExpContext ec) throws Exception {
		System.out.print("threshold");
		System.out.print("\t");
		System.out.print("1 - specificity");
		System.out.print("\t");
		System.out.println("sensitivity");
		System.out.println("0\t0\t0");

		// total true values: colors t0 = 1858, alexnet/40 is 161
		for (int thresh = 1; thresh <= 20; thresh++) {
			ec.filterThreshold = ec.queryThreshold * ((float) thresh / 20);
			doSurrogateQueries(ec, ec.noOfResultsAtThreshold);
		}
	}

	private static void createDataRep(ExpContext ec) throws Exception,
			FileNotFoundException, IOException {

		DistributionPivotsChooser<CartesianPoint> distPivsCh = new DistributionPivotsChooser<>(
				ec.getPivots(), ec.metric);

		final List<CartesianPoint> testData = ec.getBigData();
		distPivsCh.setWitnesses(testData);

		StoredExclusions se = new StoredExclusions();

		int size = testData.size();
		se.dist1s = new float[size];
		se.dist2s = new float[size];
		se.piv1s = new short[size];
		se.piv2s = new short[size];
		for (int i = 0; i < size; i++) {
			if (i % 100 == 0) {
				System.out.println("doing " + i);
			}
			CartesianPoint d = testData.get(i);
			DataPoint<CartesianPoint> x = distPivsCh.getDataPoint(d);
			se.dist1s[i] = (float) x.apex[0];
			se.dist2s[i] = (float) x.apex[1];
			se.piv1s[i] = (short) x.refs[0];
			se.piv2s[i] = (short) x.refs[1];
		}
		OutputStream fw = new FileOutputStream(temp_file_name);
		ObjectOutputStream oos = new ObjectOutputStream(fw);
		oos.writeObject(se);
		oos.close();
		//
		// for (CartesianPoint dat : testData) {
		// DataPoint<CartesianPoint> dp = d.getDataPoint(dat);
		// if (dp == null) {
		// throw new Exception(" null!");
		// }
		// System.out.print(dp.refs[0] + "\t");
		// System.out.print(dp.refs[1] + "\t");
		// System.out.print(dp.dists[0] + "\t");
		// System.out.print(dp.dists[1]);
		// System.out.println();
		// if (dp.refs[0] == dp.refs[1]) {
		// System.out.println("pivs pair");
		// }
		// }
	}

}
