package extreme_fourpoint;

import java.util.ArrayList;
import java.util.List;

import searchStructures.SearchIndex;
import searchStructures.VPTree;
import testloads.TestContext;
import testloads.TestContext.Context;
import coreConcepts.CountedMetric;
import coreConcepts.Metric;
import eu.similarity.msc.data.cartesian.CartesianPoint;
import extreme_fourpoint.RefPointsSelector.DataPoint;

public class ExtremeFourPoint<T> extends SearchIndex<T> {

	// 0,1 or 2 depending on which SISAP threshold we're using
	private static final int SAMPLE_THRESHOLD = 0;
	private static int NO_OF_SAMPLES = 2000;
	private static int NO_OF_PIVOTS = 128;
	private static double SEARCH_THRESHOLD;
	private static String DATA_SET = "NASA";

	public static String showParams() {
		StringBuffer sb = new StringBuffer();
		sb.append(DATA_SET + "_");
		sb.append(nChoose2(NO_OF_PIVOTS) + "_");
		sb.append(NO_OF_SAMPLES + "_");
		sb.append(SAMPLE_THRESHOLD);
		return sb.toString();
	}

	List<T> pivots;
	List<DataPoint<T>> xpTable;
	FourPointPivotsChooser<T> pc;

	/**
	 * exclusion mechanism is based on the four-point property using a finite
	 * set of pre-selected pivots
	 * 
	 * each datum is allocated a single pair of these pivots and a 2D coordinate
	 * is generated using them; these are stored in a table (xpPivotTable)
	 * 
	 * each query is allocated a 2D coordinate for each pivot pair, and if that
	 * 2D coordinate is more than the threshold in 2D plane then the data point
	 * can't be a solution
	 * 
	 * @param pivots
	 * @param samples
	 * @param dataset
	 * @param metric
	 */
	ExtremeFourPoint(List<T> pivots, List<T> dataset, Metric<T> metric) {
		super(null, metric);
		this.pivots = pivots;

		long t0 = System.currentTimeMillis();
		this.pc = new FourPointPivotsChooser<>(pivots, metric,
				SEARCH_THRESHOLD, true, 0);
		System.out.println("set pivot pairs in"
				+ (System.currentTimeMillis() - t0));
		this.pc.setWitnesses(dataset.subList(0, NO_OF_SAMPLES));

		this.pc.scorePivotPairs(dataset.subList(NO_OF_SAMPLES,
				NO_OF_SAMPLES + 2000));

		this.xpTable = new ArrayList<>();

		System.out.println("allocating 2d data points");
		int datNo = 0;
		for (T datum : dataset) {
			if (datNo++ % 1000 == 0) {
				System.out.println("point " + datNo);
			}
			DataPoint<T> d = this.pc.getDataPoint(datum);
			this.xpTable.add(d);
		}
	}

	@Override
	public List<T> thresholdSearch(T query, double t) {

		List<T> res = new ArrayList<>();

		double[] qToPivDists = new double[this.pivots.size()];
		for (int i = 0; i < qToPivDists.length; i++) {
			final double distance = this.metric.distance(query, pivots.get(i));
			qToPivDists[i] = distance;
			if (distance <= t) {
				res.add(this.pivots.get(i));
			}
		}

		for (DataPoint<T> d : this.xpTable) {
			double[] pivAp = d.apex;

			int i = d.refs[0];
			int j = d.refs[1];

			double[] qDists = { qToPivDists[i], qToPivDists[j] };

			int pivId = d.pivotId;

			double[] qAp = pc.pivotApex(pivId, qDists);

			/*
			 * this is the exclusion test
			 */
			if (l2(pivAp, qAp) <= t) {
				// notify pivot of failure

				if (this.metric.distance(query, d.datum) < t) {
					res.add(d.datum);
				}
			}
		}
		return res;
	}

	@Override
	public String getShortName() {
		return "xpFourPt";
	}

	public static double l2(double[] a, double[] b) {
		double a0 = a[0] - b[0];
		double a1 = a[1] - b[1];

		return Math.sqrt(a0 * a0 + a1 * a1);
	}

	public static Metric<double[]> getL2() {
		return new Metric<double[]>() {

			@Override
			public double distance(double[] x, double[] y) {
				return l2(x, y);
			}

			@Override
			public String getMetricName() {
				return null;
			}

		};
	}

	//
	// public int[] getPivotUse(int i, int j) {
	// int[] res = new int[2];
	// Pivot selectedPivot = (Pivot) this.pivotPairs[i][j];
	// res[0] = selectedPivot.successes;
	// res[1] = selectedPivot.failures;
	// return res;
	// }

	public static void main(String[] a) throws Exception {
		/*
		 * class tester only
		 */

		Context c = (DATA_SET.equals("COLORS")) ? Context.colors : Context.nasa;
		System.out.println("doing " + c);
		TestContext tc = new TestContext(c);

		double[] ts = tc.getThresholds();
		SEARCH_THRESHOLD = ts[SAMPLE_THRESHOLD];
		// System.out.println(tc.getThreshold());
		CountedMetric<CartesianPoint> cm = new CountedMetric<>(tc.metric());

		tc.setSizes(tc.dataSize() / 10, 0);

		// testVpt(tc);

		List<CartesianPoint> l = tc.getDataCopy();

		// List<CartesianPoint> pivs = Util_ISpaper.getFFT(l.subList(0, 5000),
		// tc.metric(), noOfRefPoints);
		List<CartesianPoint> pivs = l.subList(0, NO_OF_PIVOTS);
		List<CartesianPoint> dat = l.subList(NO_OF_PIVOTS, l.size());

		long t0 = System.currentTimeMillis();
		ExtremeFourPoint<CartesianPoint> efp = new ExtremeFourPoint<>(pivs,
				dat, cm);
		cm.reset();
		System.out.println("build time" + "\t"
				+ (System.currentTimeMillis() - t0));

		int res1 = 0;
		final double threshold = tc.getThreshold();
		final List<CartesianPoint> queries = tc.getQueries();

		t0 = System.currentTimeMillis();
		for (CartesianPoint q : queries) {
			res1 += efp.thresholdSearch(q, threshold).size();
		}
		System.out.println("query time" + "\t"
				+ (System.currentTimeMillis() - t0));
		System.out.println("got " + res1 + " results with "
				+ (cm.reset() / queries.size()) + " metric calls");

	}

	private static void testVpt(TestContext tc) {
		List<CartesianPoint> dataCopy = tc.getDataCopy();
		long t0 = System.currentTimeMillis();
		CountedMetric cm = new CountedMetric(tc.metric());
		VPTree<CartesianPoint> vpt = new VPTree<>(dataCopy, cm);
		cm.reset();
		System.out.println(System.currentTimeMillis() - t0);
		int res = 0;
		final double threshold = tc.getThreshold();
		for (CartesianPoint q : tc.getQueries()) {
			res += (vpt.thresholdSearch(q, threshold)).size();
		}
		System.out.println(res);
		System.out.println(System.currentTimeMillis() - t0);
		System.out.println(cm.reset() / tc.getQueries().size());

		dataCopy = tc.getDataCopy();
		t0 = System.currentTimeMillis();
		res = 0;
		for (CartesianPoint q : tc.getQueries()) {
			for (CartesianPoint p : dataCopy) {
				if (tc.metric().distance(p, q) < threshold) {
					res++;
				}
			}
		}
		System.out.println(res);
		System.out.println(System.currentTimeMillis() - t0);

	}

	public static int nChoose2(int n) {
		return (n * (n - 1)) / 2;
	}
}
