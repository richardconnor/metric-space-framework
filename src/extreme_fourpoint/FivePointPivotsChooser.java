package extreme_fourpoint;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import searchStructures.VPTree;
import sisap_2017_experiments.NdimSimplex;
import coreConcepts.Metric;

public class FivePointPivotsChooser<T> extends RefPointsSelector<T> {

	private double SEARCH_THRESHOLD;

	private class Pivot {

		int pivotId;
		List<double[]> sampleData;
		VPTree<double[]> sampleIndex;
		private NdimSimplex<T> simp;
		int[] pivIds;

		int successes;
		int failures;

		public Pivot(int[] pivIds, int pivotId) {
			this.pivIds = pivIds;
			this.pivotId = pivotId;
			this.successes = 0;
			this.failures = 0;
			List<T> pivs = new ArrayList<>();
			for (int id : pivIds) {
				pivs.add(refPoints.get(id));
			}
			this.simp = new NdimSimplex<>(metric, pivs);
		}

		@SuppressWarnings("synthetic-access")
		public int density(T point, double[] pointToPivDists) {

			// find coordinates in plot

			double[] dists = new double[pivIds.length];
			for (int id = 0; id < pivIds.length; id++) {
				dists[id] = pointToPivDists[this.pivIds[id]];
			}

			double[] ap = this.simp.getApex(dists);

			List<double[]> nn = this.sampleIndex.thresholdSearch(ap,
					SEARCH_THRESHOLD);

			return nn.size();
		}

		public double nnRadius(T point, double[] pointToPivDists, int nn) {

			// find coordinates in plot

			double[] dists = new double[pivIds.length];
			for (int id = 0; id < pivIds.length; id++) {
				dists[id] = pointToPivDists[this.pivIds[id]];
			}

			double[] ap = this.simp.getApex(dists);

			List<Integer> nns = this.sampleIndex.nearestNeighbour(ap, nn);
			double maxRad = 0;
			for (int i : nns) {
				double d = ExtremeFourPoint.getL2().distance(
						this.sampleData.get(i), ap);
				maxRad = Math.max(maxRad, d);
			}

			return maxRad;
		}

		public double[] getApex(double[] dists) {
			return this.simp.getApex(dists);
		}

		@SuppressWarnings("synthetic-access")
		public void setSample(List<T> sample, double[][] distTable) {
			this.sampleData = new ArrayList<>();
			for (int samp = 0; samp < sample.size(); samp++) {

				double[] dists = new double[pivIds.length];
				for (int id = 0; id < pivIds.length; id++) {
					dists[id] = distTable[samp][this.pivIds[id]];
				}

				double[] ap = this.simp.getApex(dists);

				this.sampleData.add(ap);
			}

			this.sampleIndex = new VPTree<>(this.sampleData,
					ExtremeFourPoint.getL2());
		}
	}

	List<Pivot> pivots;
	private boolean SEARCH_BY_THRESHOLD = false;
	private int K_FOR_KNN;

	public FivePointPivotsChooser(List<T> refPoints, Metric<T> metric,
			double threshold, boolean searchByThreshold, int kForkNN) {
		super(refPoints, metric);
		pivots = new ArrayList<>();
		this.SEARCH_THRESHOLD = threshold;
		if (searchByThreshold) {
			this.SEARCH_BY_THRESHOLD = true;
		}
		this.K_FOR_KNN = kForkNN;
	}

	@Override
	public void setWitnesses(List<T> witnesses) {

		double[][] sampleToPivDists = new double[witnesses.size()][refPoints
				.size()];

		for (int samp = 0; samp < witnesses.size(); samp++) {
			for (int piv = 0; piv < refPoints.size(); piv++) {
				sampleToPivDists[samp][piv] = metric.distance(
						witnesses.get(samp), refPoints.get(piv));
			}
		}

		for (int i = 0; i < refPoints.size() - 2; i++) {
			for (int j = i + 1; j < refPoints.size() - 1; j++) {
				for (int k = j + 1; k < refPoints.size(); k++) {
					int[] ids = { i, j, k };
					final Pivot pivot = new Pivot(ids, pivots.size());
					pivot.setSample(witnesses, sampleToPivDists);
					pivots.add(pivot);
				}
			}
		}
	}

	@Override
	public DataPoint<T> getDataPoint(T datum) {
		DataPoint<T> result = new DataPoint<>();

		result.datum = datum;

		double[] refDists = getRefDists(datum);

		Pivot p = getBestPivot(datum, refDists, result, SEARCH_BY_THRESHOLD);

		result.pivotId = p.pivotId;
 
		result.refs = p.pivIds;;

//		double[] dds = { refDists[p.pivId1], refDists[p.pivId2] };
		double[] dds = new double[p.pivIds.length];
		for( int i  =0; i < dds.length; i++){
			dds[i] = refDists[p.pivIds[i]];
		}
		result.dists = dds;
		result.apex = p.getApex(dds);

		return result;
	}

	public double[] getRefDists(T datum) {
		double[] pointToPivDists = new double[refPoints.size()];
		for (int i = 0; i < refPoints.size(); i++) {
			pointToPivDists[i] = metric.distance(datum, refPoints.get(i));
		}
		return pointToPivDists;
	}

	/**
	 * @param datum
	 * @param refDists
	 * @param dp
	 *            horrible hack, passing this in just to get side-effect to
	 *            track density outside the context
	 * @return
	 */
	private Pivot getBestPivot(T datum, double[] refDists, DataPoint<T> dp,
			boolean threshold) {

		double bestDensity = Double.MAX_VALUE;
		double bestRadius = 0;
		Pivot bestPivot = null;
		for (Pivot piv : this.pivots) {
			double d = threshold ? piv.density(datum, refDists) : piv.nnRadius(
					datum, refDists, K_FOR_KNN);
			if (threshold ? d <= bestDensity : d >= bestRadius) {
				bestDensity = d;
				bestRadius = d;
				bestPivot = piv;
				dp.density = (int) d;
				dp.radius = d;
			}
		}

		return bestPivot;
	}

	public double[] pivotApex(int pivotId, double[] dists) {
		return this.pivots.get(pivotId).getApex(dists);
	}

	public void writePivotDistanceTable(String filename) throws IOException {
		float[][] dists = new float[this.refPoints.size()][this.refPoints
				.size()];
		for (int i = 0; i < this.refPoints.size() - 1; i++) {
			for (int j = i + 1; j < this.refPoints.size(); j++) {
				double d = this.metric.distance(this.refPoints.get(i),
						this.refPoints.get(j));
				dists[i][j] = (float) d;
			}
		}
		OutputStream fw = new FileOutputStream(filename);
		ObjectOutputStream oos = new ObjectOutputStream(fw);
		oos.writeObject(dists);
		oos.close();
	}
}
