package extreme_fourpoint;

import java.util.List;

import sisap_2017_experiments.NdimSimplex;
import coreConcepts.Metric;
import extreme_fourpoint.RefPointsSelector.DataPoint;

public class MaxMinPivotsChooser<T> extends GeometricPivotsChooser<T> {

	MaxMinPivotsChooser(List<T> refPoints, Metric<T> metric) {
		super(refPoints, metric);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void assign1stReferenceToData(List<T> allData) {
		for (T datum : allData) {
			DataPoint<T> dp = this.results.get(datum);

			for (int i = 0; i < this.refPoints.size(); i++) {
				double smallestDist = Double.MAX_VALUE;

				double dist = this.metric
						.distance(datum, this.refPoints.get(i));
				if (dist < smallestDist) {
					smallestDist = dist;
					dp.refs[1] = i;
					dp.dists[1] = dist;
				}
			}
		}
	}

	@Override
	protected void assign2ndReferenceToData(List<T> allData) {
		for (T datum : allData) {
			DataPoint<T> dp = this.results.get(datum);

			int p1 = dp.refs[0];

			for (int i = 0; i < this.refPoints.size(); i++) {
				double biggestDist = Double.MAX_VALUE;
				if (i != p1) {

					double dist = this.metric.distance(datum,
							this.refPoints.get(i));
					if (dist < biggestDist) {
						biggestDist = dist;
						dp.refs[1] = i;
						dp.dists[1] = dist;
					}
				}
			}
		}

	}

}
