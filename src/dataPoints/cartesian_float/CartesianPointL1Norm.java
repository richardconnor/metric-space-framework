package dataPoints.cartesian_float;

public class CartesianPointL1Norm extends CartesianPoint {

	public CartesianPointL1Norm(float[] point) {
		super(point);

		float acc = 0;
		for (float f : point) {
			acc += f;
		}
		if (acc == 0) {
			throw new RuntimeException("can't normalise zeroed point");
		}
		for (int i = 0; i < point.length; i++) {
			point[i] = point[i] / acc;
		}
	}

}
