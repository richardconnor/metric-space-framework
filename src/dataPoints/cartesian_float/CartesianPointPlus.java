package dataPoints.cartesian_float;

import java.util.List;

import eu.similarity.msc.util.OrderedList;

public class CartesianPointPlus extends CartesianPoint {

	private double fieldTotal = -1;
	private double magnitude = -1;
	private double magnitudeSq = -1;
	private List<Integer> magOrdering;

	public CartesianPointPlus(float[] point) {
		super(point);
	}


	/**
	 * @return the vector magnitude of the point; used in Cosine distance;
	 *         lazily evaluates the calculation
	 */
	public double getMagnitude() {
		if (this.magnitude == -1) {
			getMagnitudeSq();
			this.magnitude = Math.sqrt(this.magnitudeSq);
		}
		return this.magnitude;
	}

	/**
	 * @return the square of the vector magnitude of the point; used in
	 *         Tanimoto; lazily evaluates the calculation distance
	 */
	public double getMagnitudeSq() {
		if (this.magnitudeSq == -1) {
			double sum = 0;
			for (double d : this.point) {
				sum += d * d;
			}
			this.magnitudeSq = sum;
		}
		return this.magnitudeSq;
	}

	/**
	 * @return the total of the array values
	 */
	private double getFieldTotal() {
		if (this.fieldTotal != -1) {
			return this.fieldTotal;
		} else {
			double sum = 0;
			for (float d : this.point) {
				sum += d;
			}

			this.fieldTotal = sum;
			return sum;
		}
	}

	/**
	 * @return a list of integers, giving the dimensions, in order, according to
	 *         the magnitude of the array values
	 */
	@SuppressWarnings("boxing")
	public List<Integer> magnitudeOrdering() {
		if (this.magOrdering == null) {
			OrderedList<Integer, Double> ord = new OrderedList<>(
					this.point.length);
			int dim = 0;
			for (double v : this.point) {
				ord.add(dim, v);
				dim++;
			}
			this.magOrdering = ord.getList();
		}
		return this.magOrdering;
	}

}
