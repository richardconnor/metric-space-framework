package dataPoints.cartesian_float;

public class CartesianPointLogistic extends CartesianPoint {

	public CartesianPointLogistic(float[] point) {
		super(point);
		for (int i = 0; i < this.point.length; i++) {
			this.point[i] = logisticFunction(this.point[i]);
		}
	}

	private static float logisticFunction(float x) {
		return (float) (0.5 + 0.5 * Math.tanh(x / 2));
	}
}
