package dataPoints.cartesian_float;

public class CartesianPointL2Norm extends CartesianPoint {

	public CartesianPointL2Norm(float[] point) {
		super(point);

		float acc = 0;
		for (float f : point) {
			acc += f * f;
		}
		if (acc == 0) {
			throw new RuntimeException("can't normalise zeroed point");
		}
		float mag = (float) Math.sqrt(acc);
		for (int i = 0; i < point.length; i++) {
			point[i] = point[i] / mag;
		}
	}

}
