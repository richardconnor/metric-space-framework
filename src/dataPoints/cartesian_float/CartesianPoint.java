package dataPoints.cartesian_float;

/**
 * 
 * a class representing Cartesian points, subclasses have utility methods for
 * memo-ising computed data to make various metrics more efficient
 * 
 * @author Richard Connor
 * 
 */
public class CartesianPoint {

	protected float[] point;

	/**
	 * Create a new CartesianPoint based on an array of doubles
	 * 
	 * @param point
	 *            the array of doubles
	 */
	public CartesianPoint(float[] point) {
		this.point = point;
	}

	/**
	 * @return the array used to create the point
	 */
	public float[] getPoint() {
		return this.point;
	}

	/**
	 * @return a String representing the point in a CSV file format
	 */
	public String toCsvString() {
		StringBuffer b = new StringBuffer("point"); //$NON-NLS-1$

		for (double d : this.point) {
			b.append("," + d); //$NON-NLS-1$
		}
		return b.toString();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuffer res = new StringBuffer("Point: ["); //$NON-NLS-1$

		for (double d : this.point) {
			res.append(d + " ,");
		}
		res.delete(res.length() - 2, res.length());
		res.append(']');

		return res.toString();
	}

}
