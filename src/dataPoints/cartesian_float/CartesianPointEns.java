package dataPoints.cartesian_float;

public class CartesianPointEns extends CartesianPointL1Norm {

	private static final double LOG_2 = Math.log(2);
	private double entropy = -1;
	private float[] logTerms;

	public CartesianPointEns(float[] point) {
		super(point);
	}

	/**
	 * @return the complexity of this point; lazily evaluates the calculation
	 */
	public double getEntropy() {
		if (this.entropy == -1) {
			double acc = 0;
			for (float d : this.point) {
				if (d != 0) {
					acc -= d * Math.log(d);
				}
			}
			this.entropy = acc;
		}
		return this.entropy;
	}

	/**
	 * @return an array of log terms (-x.log(x) to log base 2) for each
	 *         dimension; used in SED and Jensen-Shannon; lazily evaluates the
	 *         calculation
	 */
	public float[] getLog2Terms() {
		if (this.logTerms == null) {
			this.logTerms = new float[this.point.length];
			for (int i = 0; i < this.point.length; i++) {
				double d = this.point[i];
				if (d == 0) {
					this.logTerms[i] = 0;
				} else {
					this.logTerms[i] = (float) (-d * (Math.log(d) / LOG_2));
				}
			}
		}
		return this.logTerms;
	}

	/**
	 * @return the complexity of this point; lazily evaluates the calculation
	 */
	public double getComplexity() {
		return Math.pow(Math.E, this.getEntropy());
	}
}
