package dataPoints.cartesian_float;

import coreConcepts.Metric;

/**
 * @author newrichard
 *
 * @param <T>
 */
public class Triangular<T extends CartesianPointL1Norm> implements Metric<T> {

	@Override
	public double distance(T x, T y) {
		return probDistance(x.getPoint(), y.getPoint());
	}

	static private double probDistance(float[] x, float[] y) {
		double acc = divergenceOver2(x, y);
		return Math.sqrt(Math.abs(acc));
	}

	/*
	 * this is the correct semantics, optimised semantics used below instead
	 */
	private static double divergence(float[] x, float[] y) {
		double acc = 0;
		for (int i = 0; i < x.length; i++) {
			final double x_i = x[i];
			final double y_i = y[i];
			if (x_i == 0 || y_i == 0) {
				acc += (x_i + y_i);
			} else {
				double bottom = x_i + y_i;
				double topRoot = x_i - y_i;
				acc += (topRoot * topRoot) / bottom;
			}
		}
		return acc;
	}

	/**
	 * redefined divergence according to unpublished paper on four metrics...
	 * 
	 * @param x
	 * @param y
	 * @return
	 */
	private static double divergenceOver2(float[] x, float[] y) {
		double acc = 0;
		for (int i = 0; i < x.length; i++) {
			final double x_i = x[i];
			final double y_i = y[i];
			final double thing = (x_i * y_i) / (x_i + y_i);
			if (!Double.isNaN(thing)) {
				acc += thing;
			}
		}
		return 1 - acc * 2;
	}

	@Override
	public String getMetricName() {
		return "tri";
	}

}
