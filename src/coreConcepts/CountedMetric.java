package coreConcepts;

import coreConcepts.Metric;

public class CountedMetric<T> implements Metric<T> {

	private long count;
	private Metric<T> m;

	public CountedMetric(Metric<T> m) {
		this.count = 0;
		this.m = m;
	}

	@Override
	public double distance(T x, T y) {
		this.count++;
		return this.m.distance(x, y);
	}

	@Override
	public String getMetricName() {
		return m.getMetricName();
	}

	public long resetL() {
		long res = this.count;
		this.count = 0;
		return res;
	}

	public int reset() {
		long res = this.count;
		this.count = 0;
		return (int) res;
	}

}
