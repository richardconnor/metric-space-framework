% this started April 2016 to investigate the offset spread and think about
% LSH probabilities for different Hilbert spaces

use_RIG_RetClass=false; %set false for using Franco's FIG_Rect class

centerPCA=true;
%%
% load section2.mat (\metric-space-framework\MATLAB\high dimensional space)
%'execute' setupColors.m
% setap10dim.m
%%

if(use_RIG_RetClass)
    legendFontSize = 32;
    axisLabelFontSize = 30;
    titleFontSize = 34;
    ticksFontSize = 30;
    pivotPointsTextSize = ticksFontSize - 4;
    labelFontWeight = 'bold';
    circleLineWidth = 2;
    pivotSize = 250;
    discSize = 100;
    linesWidth = 5;
      
else
    legendFontSize = 18;
    axisLabelFontSize = 16;
    titleFontSize = 18;
    ticksFontSize = 16;
    pivotPointsTextSize = ticksFontSize - 4;
    labelFontWeight = 'bold';
    circleLineWidth = 0.7;
    pivotSize = 75;
    discSize = 18;
    linesWidth = 2.5;
end



possibleAnalyses = {'', 'horizontal','vertical','circle','farLeftDist','pcaFirstPrincComp', 'pcaSecondPrincComp'}
%{'','pcaFirstPrincComp', 'pcaSecondPrincComp'};%,
%;%,
%
baseFigIdx = 500;



%analysisIdx = 2;


number = 500;

% first calcalate the actual XY coordinates in the plan
% these will always be plotted as absolutes
coords = zeros(2,number);
for i = 1 : number
    d1 = dists(i,1);
    d2 = dists(i,2);
    
    %a^2 = b^2 + c^2 - 2bc cosA
    % so.... cosA = (b^2 +cl c^2 - a^2)/2bc
    cos_piv_1 = (d1 * d1 + pDist * pDist - d2 * d2) / (2 * pDist);
    x_offset = cos_piv_1;
    y_offset = sqrt(d1 * d1 - x_offset * x_offset);
    
    coords(1,i) = x_offset - pDist/2;
    coords(2,i) = y_offset;
end


% calculate median x and y positions
medX = median(coords(1,:));
medY = median(coords(2,:));

% now calculate subsets for division of original space so that we can play
% with that later, not currently in any use
leftPartition = zeros(2,number);
rightPartition = zeros(2,number);
noOfLeftPoints = 0;
noOfRightPoints = 0;

Xorig=coords';
mu=mean(Xorig);
[W, Xrot, LATENT, EXPLAINED]= pca(Xorig,'Centered', centerPCA);%,  'Algorithm', 'svd'
% 'Centered'=true--> (Xorig-mu)=Xrot*W'
% 'Centered'=false--> Xorig=Xrot*W'
LATENT
median_afterPCA=median(Xrot);
newmedX= median_afterPCA(1);
newmedY= median_afterPCA(2);


for analysisIdx=2:length(possibleAnalyses)
    figIdx = baseFigIdx + analysisIdx;
    
    analysis = possibleAnalyses(analysisIdx);
    for i = 1 : number
        leftCondition = true;
        if strcmp(analysis,'vertical')
            leftCondition = coords(1,i) < medX;
        elseif strcmp(analysis,'horizontal')
            leftCondition = coords(2,i) < medY;
        elseif strcmp(analysis,'circle')
            
        elseif strcmp(analysis,'farLeftDist')
            
        elseif strcmp(analysis,'pcaFirstPrincComp')
            leftCondition = Xrot(i,2) < newmedY;
            
        elseif strcmp(analysis,'pcaSecondPrincComp')
            leftCondition = Xrot(i,1) < newmedX;
        end;
        
        if leftCondition
            noOfLeftPoints = noOfLeftPoints + 1;
            leftPartition(1,noOfLeftPoints) = coords(1,i);
            leftPartition(2,noOfLeftPoints) = coords(2,i);
        else
            noOfRightPoints = noOfRightPoints + 1;
            rightPartition(1,noOfRightPoints) = coords(1,i);
            rightPartition(2,noOfRightPoints) = coords(2,i);
        end
    end
    
    % eventually we will also want cover radii for these partitions
    leftPivot = [-pDist/2,0];
    rightPivot = [pDist/2,0];
    
    lCr = 0;
    lMr = 10;
    for i = 1 : noOfLeftPoints
        lCr = max(lCr,euc(leftPivot,leftPartition(:,i)'));
        lMr = min(lMr,euc(leftPivot,leftPartition(:,i)'));
    end
    
    rCr = 0;
    rMr = 10;
    for i = 1 : noOfRightPoints
        rCr = max(rCr,euc(rightPivot,rightPartition(:,i)'));
        rMr = min(rMr,euc(rightPivot,rightPartition(:,i)'));
    end
    
    %now calculate points with specific properties - keypoints are points that,
    %were they to be queries, would not require to search the opposing subspace
    keyPoints = zeros(2,number);
    noOfKeyPoints = 0;
    
    centre = [medX,medY]; %%
    
    %find median distance from centre
    centreDists = zeros(1,number);
    for i = 1 : number
        centreDists(i) = euc(coords(:,i)',[medX,medY]);
    end
    medianDistFromCentre = median(centreDists);
    
    
    % let's find distances from the bottom-left corner of the diagram
    minX = min(coords(1,:));
    minY = min(coords(2,:));
    maxX = max(coords(1,:));
    maxY = max(coords(2,:));
    botLeft = [minX,maxY];
    botLeftDists = zeros(1,number);
    for i = 1 : number
        botLeftDists(i) = euc(coords(:,i)',botLeft);
    end
    medianBotLeftDist = median(botLeftDists);
    
    eigenvalue='';
    for i = 1 : number
        condition = false;
        
        if strcmp(analysis,'vertical')
            condition = abs(coords(1,i) - medX) > threshold;
        elseif strcmp(analysis,'horizontal')
            condition = abs(coords(2,i) - medY) > threshold;
        elseif strcmp(analysis,'circle')
            
            condition = abs(euc(coords(:,i)',centre) - medianDistFromCentre) > threshold;
        elseif strcmp(analysis,'farLeftDist')
            %         take an arbitrary point at (minX, minY) and partition by
            %         distance from that; assume
            
            
            condition = abs(euc(coords(:,i)',botLeft) - medianBotLeftDist) > threshold;
            
        elseif strcmp(analysis,'pcaFirstPrincComp') %hyperplane parallel to 1� princ comp, i.e. orthogonal to the secon princ comp
            condition = abs(Xrot(i,2) - newmedY) > threshold;
           % eigenvalue=strcat('-eig ',num2str(round(LATENT(1),3)),' ');
            
        elseif strcmp(analysis,'pcaSecondPrincComp')%hyperplane parallel to 2� princ comp, i.e. orthogonal to the first princ comp
            condition = abs(Xrot(i,1) - newmedX) > threshold;
            % eigenvalue=strcat('-eig ',num2str(round(LATENT(2),3)),' ');
        end;
        
        if condition
            noOfKeyPoints = noOfKeyPoints + 1;
            keyPoints(1,noOfKeyPoints) = coords(1,i);
            keyPoints(2,noOfKeyPoints) = coords(2,i);
        end
    end
    
    
    h = figure(figIdx);
    clf;
    
    if(use_RIG_RetClass)
        scaleFactor = 2;
        FIG_Rect(figIdx, scaleFactor, scaleFactor);
    end
    title_text = strcat(analysis, '');
    %strcat(title_text,num2str(noOfKeyPoints));
    title(strcat(title_text,eigenvalue,' - ', dataSeriesName), 'FontSize', titleFontSize, 'FontWeight', labelFontWeight);
    xlabel('X', 'FontSize', axisLabelFontSize, 'FontWeight', labelFontWeight);
    ylabel('altitude from line (p_1,p_2)', 'FontSize', axisLabelFontSize, 'FontWeight', labelFontWeight);
    whitespace = 1.1;
    xSpread = max(coords(1,:))-min(coords(1,:));
    ySpread = max(coords(2,:));
    bigger  = max(xSpread,ySpread) * whitespace;
    % axis([-bigger/2,bigger/2,0,bigger]);
    yAxisLimit = max(coords(2,:))*whitespace;
    xAxisMin = min(coords(1,:))*whitespace;
    xAxisMax =  max(coords(1,:))*whitespace;
    axis([min(coords(1,:))*whitespace,max(coords(1,:))*whitespace,0,max(coords(2,:))*whitespace]);
    set(gca, 'FontSize', ticksFontSize);
    hold on;
    
    % plot the points
    % this line is just to test and won't usually be uncommented
    p1 = scatter(coords(1,:),coords(2,:),discSize,[0,0,0], 'LineWidth', circleLineWidth);%50,[0,0,0]);
    
    keys = scatter(keyPoints(1,1:noOfKeyPoints),keyPoints(2,1:noOfKeyPoints),discSize,[0,0,0],'filled', 'LineWidth', circleLineWidth);%50,[0,0,0],'filled');
    
    pivotsHandler = scatter([-pDist/2,pDist/2],[0,0],pivotSize,[0,0,0], 'filled');
    text(-pDist/2, 0.02, 'p_1', 'HorizontalAlignment', 'center', 'VerticalAlignment', 'bottom', 'FontSize', pivotPointsTextSize);
    text(pDist/2, 0.02, 'p_2', 'HorizontalAlignment', 'center', 'VerticalAlignment', 'bottom', 'FontSize', pivotPointsTextSize);
    
    p2_label = strcat('exclusive queries, n = {}',num2str(noOfKeyPoints));
    
    
    grayVal = 0.75;
    lineColor = [grayVal grayVal grayVal];
    if strcmp(analysis,'vertical')
        plot(linspace(medX-threshold,medX-threshold,100),linspace(0,yAxisLimit,100),'-','Color', lineColor, 'LineWidth', linesWidth);
        plot(linspace(medX+threshold,medX+threshold,100),linspace(0,yAxisLimit,100),'-','Color', lineColor, 'LineWidth', linesWidth);
    elseif strcmp(analysis,'horizontal')
        plot(linspace(xAxisMin,xAxisMax,100),linspace(medY-threshold,medY-threshold,100),'-','Color', lineColor, 'LineWidth', linesWidth);
        plot(linspace(xAxisMin,xAxisMax,100),linspace(medY+threshold,medY+threshold,100),'-','Color', lineColor, 'LineWidth', linesWidth);
    elseif strcmp(analysis,'circle')
        plotCircle(centre, medianDistFromCentre - threshold, linesWidth, lineColor);
        plotCircle(centre, medianDistFromCentre + threshold, linesWidth, lineColor);
    elseif strcmp(analysis,'farLeftDist')
        plotCircle(botLeft, medianBotLeftDist - threshold, linesWidth, lineColor);
        plotCircle(botLeft, medianBotLeftDist + threshold, linesWidth, lineColor);
    elseif strcmp(analysis,'pcaFirstPrincComp')
        xRotAxisMin = min(Xrot(:,1))*whitespace;
        xRotAxisMax =  max(Xrot(:,1))*whitespace;
        tx=linspace(xRotAxisMin,xRotAxisMax,100);
        ty_minus=linspace(newmedY-threshold,newmedY-threshold,100);
        ty_plus=linspace(newmedY+threshold,newmedY+threshold,100);
        if(centerPCA)           
            plot(W(1,1)*tx+W(1,2)*ty_plus+mu(1),W(2,1)*tx+W(2,2)*ty_plus+mu(2),'-','Color', lineColor, 'LineWidth', linesWidth);
            plot(W(1,1)*tx+W(1,2)* ty_minus+mu(1),W(2,1)*tx+W(2,2)* ty_minus+mu(2),'-','Color', lineColor, 'LineWidth', linesWidth);
        else
            plot(W(1,1)*tx+W(1,2)*ty_plus,W(2,1)*tx+W(2,2)*ty_plus,'-','Color', lineColor, 'LineWidth', linesWidth);
            plot(W(1,1)*tx+W(1,2)* ty_minus,W(2,1)*tx+W(2,2)* ty_minus,'-','Color', lineColor, 'LineWidth', linesWidth);
        end
        
    elseif strcmp(analysis,'pcaSecondPrincComp')
        yRotAxisMin = min(Xrot(:,2))*whitespace;
        yRotAxisMax =  max(Xrot(:,2))*whitespace;
        ty=linspace(yRotAxisMin,yRotAxisMax,100);
        tx_minus=linspace(newmedX-threshold,newmedX-threshold,100);
       tx_plus=linspace(newmedX+threshold,newmedX+threshold,100);
       if(centerPCA)
            plot(W(1,1)*tx_plus+W(1,2)*ty+mu(1),W(2,1)*tx_plus+W(2,2)*ty+mu(2),'-','Color', lineColor, 'LineWidth', linesWidth);
            plot(W(1,1)*tx_minus+W(1,2)* ty+mu(1),W(2,1)*tx_minus+W(2,2)* ty+mu(2),'-','Color', lineColor, 'LineWidth', linesWidth);
        else
            plot(W(1,1)*tx_plus+W(1,2)*ty,W(2,1)*tx_plus+W(2,2)*ty,'-','Color', lineColor, 'LineWidth', linesWidth);
            plot(W(1,1)*tx_minus+W(1,2)* ty,W(2,1)*tx_minus+W(2,2)* ty,'-','Color', lineColor, 'LineWidth', linesWidth);
        end
    end;
    
    if(use_RIG_RetClass)
        LEG = legend([p1,keys],'non-exclusive queries',p2_label, 'FontSize', legendFontSize, 'Color', 'none', 'Location', 'NorthWest')
        set(LEG, 'Color', 'none');
    else
        LEG =legend([p1,keys],'non-exclusive queries',p2_label,'Location', 'northwest');%'FontSize', legendFontSize, 'Color', 'none', 
        set(LEG, 'Color', 'w');
    end
    
    M = findobj(LEG,'type','patch'); % Find objects of type 'patch'
    set(M,'MarkerSize', sqrt(discSize)+5) %Calculate marker size based on size of scatter points
    hold off;
    
    % p2_label = strcat('exclusive queries, n = {}',num2str(no_of_discards));
    % legend([p1,p2],'non-exclusive queries',p2_label)
    % pic_filename = 'partition_plot2_';
    
    % pic_filename = strcat(pic_filename,'hilbert_lsh_test');
    
    % saveas(h,'lastPlot.png');
    %
    % h2 = figure;
    % title_text = 'Four point X-offsets, {}';
    % title(strcat(title_text, num2str(pDist), ' pivot separation'));
    % hold on;
    % hist = histogram(coords(1,:),'Normalization','cdf');
    % hold off;
    if(~use_RIG_RetClass)
      % axis equal
        path='fig/';
        tit=num2str(cell2mat(strcat(dataSeriesName,'-',analysis)));
        tit(ismember(tit,' ,.:;!{}')) = [];
        saveas(h,strcat(path,tit,'.pdf'));   
 end
end


% FIG_Save(502, 'AA-horizontal-color', 'pdf', 300, 90');
% FIG_Save(503, 'AA-vertical-color', 'pdf', 300, 90');
% FIG_Save(504, 'AA-circle-color', 'pdf', 300, 90');
% FIG_Save(505, 'AA-farLeft-color', 'pdf', 300, 90');
% FIG_Tile([baseFigIdx+2 baseFigIdx+3;baseFigIdx+4 baseFigIdx+5], 2);

   

    