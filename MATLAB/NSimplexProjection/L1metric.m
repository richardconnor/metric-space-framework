classdef L1metric < MetricAbstract
    %Euclidean distance- Class for Euclidean distance
    %   
    properties 
        metricname
    end
   
    methods (Static)
        function obj=L2metric()
            obj.metricname='L1';
        end
        function res = distance(a,b)
            %compute L1 distance
            res=sum(abs(a-b));
        end
          function dists = distanceToMorePoints(a,B)
            %compute the distance between the vector a and each
            %row of the matrix B
            %output row vector 
            C=bsxfun(@minus,B,a);%subtract a from each rows of B
            dists=sqrt(sum(abs(C),2))';
            
        end
    end
    
end

