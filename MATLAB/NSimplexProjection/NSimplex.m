classdef NSimplex
    % Class to construct a (n-1)-dimensional simplex given n reference object p1,..,pn and to compute the n-Simplex projection of a point
    %
    % GENERAL INFO ON THE N-SIMPLEX PROJECTION
    % If a metric space (X,d) meets the (n-1)-point property then for any n
    % objects sampled from the original space, there exists an (n-1)-dimensional
    % simplex in the Euclidean space whose edge lengths correspond to the 
    % actual distances between the objects.
    %
    % Let SIGMA=<p1,..,pn>  the simplex generated by the point p1,..pn \in (X,d)
    % and let v1,...,vn  the corresponding vertices, i.e. v1,...,vn are vectors in R^(n-1).
    % s.t. l2(vi,vj)=d(pi,pj) \forall i,j=1..n
    % We call "baseSimplex" the matrix  [v1;...;vn] (i.e. baseSimplex(i,:)=vi)
    %
    % Given the baseSimplex and a point o, and the distances d(o,pi) \forall i=1,..,n, 
    % it is possible to project the point o into the point vo\in R^n s.t.
    % l2(vo,vi)=d(o, pi) for all i
    %
    % Given two objects q and o, 
    % a baseSimplex generated from <p1,..,pn>, 
    % and the distances
    % d(q,pi) for i=1,..,n, 
    % d(o,pi) for i=1,..,n, 
    % the projected points vo, vq satisfies
    % l2(vq,vo)<=d(o,q)<=l2(vq,vo_minus)
    % where 
    % vo_minus= [vo(1:end-1),-vo(end)]
    %
    %NSimplex Properties:
    % metric           - Distance used to compare two objects in X. It should be
    %                    exends the MetricAbstract class, e.g. L2metric
    %
    % referenceObjects - nxD matrix containing the coordinates of n 
    %                    reference objects (pivots) p1,..,pn \in X used to build 
    %                    the baseSimplex 
    %                           referenceObjects(i,:)=pi;
    % 
    % baseSimplex      - n x(n-1) strictly triangular matrix
    %                    The rows are the coordinates of the vertices v1,...,vn 
    %                    of the simplex built from the set of referenceObjects (i.e., p1,..,pn).
    %                 
    %                     e.g.  |0,   0   ...           0|
    %                           |v21  0   ...           0|
    %                           |v31  v32 ...           0|
    %                           |:    :   :              |
    %                           |vn1  vn2 ...     vn(n-1)|
    %NSimplex Methods:
    %
    % NSimplex(referenceObjects, metric)- class constructor 
    %                    INPUT:
    %                     * referenceObjects: matrix containing the coordinates of n 
    %                              reference objects(each row is an object)
    %                     * metric:  the distance used to compare two objects
    %                              (should exends the MetricAbstract class, e.g. L2metric)
    %
    % 
    % 
    % getApexFromDists(dists) - Determines the Cartesian coordinates for the last vertex of a simplex,
    %                 given only the distances (dists) between the point to be projected and each reference objects
    % 
    % project(point) - Use the "baseSimplex" to project the "point", i.e. determines the Cartesian coordinates 
    %                   of the apex of an n-dimensional simplex determined given the coordinates of an (n - 1)-dimensional simplex
    %                   and the distances from the new apex to each vertex in the existing simplex                
    %                                  
    %
    % %Example:
    % nRO=10;
    % dimRO=15;
    % metric=L2metric;
    % referenceObjects=randn(nRO,dimRO);
    % mysimplex=NSimplex(referenceObjects,metric);
    % o=randn(1,dimRO);
    % q=randn(1,dimRO);
    % o_projected=mysimplex.project(o);
    % q_projected=mysimplex.project(q);
    % actualDistance=metric.distance(o,q)
    % simplexLwb=L2metric.distance(q_projected,o_projected)
    % simplexUpb=L2metric.distance(q_projected,[o_projected(1:end-1),-o_projected(end)])
    %
    % see also https://arxiv.org/pdf/1707.08370.pdf
   
    
        
    properties
        metric %Distance used to compare two objects in the original space
        referenceObjects %nxD matrix containing the coordinates of the n reference objects used to built teh simplex
        baseSimplex %n x(n-1)matrix representing a (n-1)-dimensional simplex 
    end
    
    methods 
        %% 
        function obj = NSimplex(referenceObjects, metric)
            %class constructor 
            %          INPUT:
            %             - referenceObjects: matrix containing the coordinates of n
            %                              reference objects(each row is an object)
            %             - metric:  the distance used to compare two objects
            %                              (should exends the MetricAbstract class, e.g. L2metric)
            %
            obj.metric=metric;
            obj.referenceObjects=referenceObjects;
            n= size(referenceObjects,1);
            obj.baseSimplex=zeros(n,n-1);
            obj.baseSimplex(2,1) = metric.distance(referenceObjects(2,:),referenceObjects(1,:));
            if (obj.baseSimplex(2,1) == 0)
                error('zero distance points for base simplex');
            end
            % for every further reference point, add the apex
            for k =3:n
                %distances od p_k to all previous points (p1, ..,p_(k-1)
                dists_k = zeros(1,k-1);
                for j = 1:k-1
                    dists_k(j) = metric.distance(referenceObjects(k,:), referenceObjects(j,:));
                end
                if (min(dists_k)==0)
                    error('zero distance points for base simplex!Bad choice for referenceObjects'); %TODO: skip thepivot and reduce the base 
                end
                apex = NSimplex.getApex(obj.baseSimplex(1:k-1,1:k-2), dists_k);
                obj.baseSimplex(k,1:k-1)= apex; %size(apex)=(1, k-1)
            end
            
        end
        %%
     function newApex = getApexFromDists(obj, dists)
            %Determines the Cartesian coordinates for the last vertice of a simplex,
            %given only the distances (dists) between the point to be projected and its
            %distances to each vertex of the simplex base (i.e. the rows of the baseSimplex  matrix).
      newApex =NSimplex.getApex(obj.baseSimplex,dists);
     end
      %%
     function newApex = project(obj, point)
            %Use the "baseSimplex" to project the "point"
            %     determines the Cartesian coordinates 
            %     of the apex of an n-dimensional simplex determined given the coordinates of an (n - 1)-dimensional simplex
            %     and the distances from the new apex to each vertex in the existing simplex 
            actual_metric=obj.metric;
            dists_o=actual_metric.distanceToMorePoints(point,obj.referenceObjects);
            newApex =NSimplex.getApex(obj.baseSimplex,dists_o);
     end
    end
     
    
    methods(Static=true, Hidden=true)
        function newApex=getApex(simplexMatrix,dists)
            % STATIC and Hidden method. Determines the Cartesian coordinates for the last vertex
            % of a simplex given the distances of a point o to the reference points p1,...,pn used to
            %  generate the simplex represented by the simplexMatrix
            %   INPUT:
            %   *simplexMatrix: a simplex base matrix of the form 
            %            |0    0   ...           0|
            %            |v21  0   ...           0|
            %            |v31  v32 ...           0|\in R^n x R^(n-1)
            %            |:    :   :             0|
            %            |vn1  vn2 ...     vn(n-1)|
            %   *dists: the distances between the point to be projected and each reference objects of the simplex base.
            %           dists=[d1,...,dn] \in R^1 x R^n
            %   OUTPUT
            %    * newApex: the cartesian coordinates for the point o s.t. l2(newApex,simplexMatrix(i,:))=d(o,pi)
            %               newApex \in \in R^1 x R^n;
            %
            %
                     
            n = size(dists,2);
            newApex=zeros(1,n);
            newApex(1)=dists(1);
            for k=2:n
                ldist=L2metric.distance(newApex(1:k-1), simplexMatrix(k,1:k-1)); %here size(newApex)=(1,k-1)
                d = dists(k);
                xN = simplexMatrix(k,k-1);
                yN = newApex(k-1);
                secondLastVal = yN - (d * d - ldist * ldist) / (2 * xN);
                newApex(k-1)=secondLastVal;
                lastVal = (yN * yN - secondLastVal * secondLastVal);
                
                if(isnan(lastVal)) %add ifnty case?
                    lastVal=0;
                else
                    if (lastVal<0)
                        error('Error: set of points that not satisfies the n-point property');
                    end
                newApex(k)=sqrt(lastVal);
                end
                
            end
                    
        end %getApex
    end %methods
    
end
    
