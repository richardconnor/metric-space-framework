classdef L2metric < MetricAbstract
    %Euclidean distance- Class for Euclidean distance
    %   
    properties 
        metricname
    end
   
    methods (Static)
        function obj=L2metric()
            obj.metricname='Euclidean';
        end
        function res = distance(a,b)
            %compute euclidean distance
            res=sqrt(sum((a-b).^2));
        end
         function val = squaredDistance(a,b)
            %compute squared euclidean distance
            val=sum((a-b).^2);
         end
          function dists = distanceToMorePoints(a,B)
            %compute the euclidean distance between the vector a and each
            %row of the matrix B
            %output row vector 
            C=bsxfun(@minus,B,a);%subtract a from each rows of B
            dists=sqrt(sum(C.^2,2))';
            
        end
    end
    
end

