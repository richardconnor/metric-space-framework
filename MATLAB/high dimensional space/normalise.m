function n = normalise(A)

n = A ./ sum(A);

end