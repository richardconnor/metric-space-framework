function d = h(x)

d = -x * (log(x)/log(2));

end